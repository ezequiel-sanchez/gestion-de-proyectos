<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Recordatorios */

$this->title = 'Create Recordatorios';
$this->params['breadcrumbs'][] = ['label' => 'Recordatorios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recordatorios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
