<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;
use app\controllers\ProyectosController;

$this->title = 'Seleccionar';
if (isset($model->plazo_entrega)) {
    $fechaFix = explode('-', $model->plazo_entrega);
    $fechaA = $fechaFix[0];
    $fechaM = $fechaFix[1];
    $fechaD = $fechaFix[2];
    $fechaFix = 'Plazo: ' . $fechaD . '-' . $fechaM . '-' . $fechaA;
} else {
    $fechaFix = 'Sin plazo de entrega';
}

$tiempo = ProyectosController::updateLayout($model->id, 'tiempo');
$hitosCompletados = ProyectosController::updateLayout($model->id, 'hitosCompletados');
$totalHitos = ProyectosController::updateLayout($model->id, 'totalHitos');
$objetivosCompletados = ProyectosController::updateLayout($model->id, 'objetivosCompletados');
$totalObjetivos = ProyectosController::updateLayout($model->id, 'totalObjetivos');
$tareasCompletadas = ProyectosController::updateLayout($model->id, 'tareasCompletadas');
$totalTareas = ProyectosController::updateLayout($model->id, 'totalTareas');

if (!isset($tiempo)) {
    $tiempo = 'tal';
}
if (!isset($totalHitos)) {
    $totalHitos = 0;
}

if (!isset($hitosCompletados)) {
    $hitosCompletados = 0;
}

if (!isset($objetivosCompletados)) {
    $objetivosCompletados = 0;
}

if (!isset($totalObjetivos)) {
    $totalObjetivos = 0;
}

if (!isset($tareasCompletadas)) {
    $tareasCompletadas = 0;
}

if (!isset($totalTareas)) {
    $totalTareas = 0;
}

if ($totalTareas == 0) {
    $totalTareas = 1;
}
?>
<!--<script>
    window.onload = function () {
        fixFecha(< ?= $model->plazo_entrega ?>,< ?= $model->id ?>);
    }
</script>-->

<div class="bgb my-3 w-100" onmouseenter="hoverSound('<?= $_SESSION['dir'] ?>', 'Extra_Click')">        
    <div onclick="continuar(<?= $model->id ?>)" class="borde btn-zelda p-2">

        <div class="col align-self-lg-center">
            <div class="row justify-content-center">

                <div class="col-md-9 col-sm-9">
                    <h1 class="display-4"><span class="h1"><?= $_SESSION['tipo'] ?></span><?= ucfirst($model->titulo_proyecto) ?></h1>
                </div>

                <div class="col-md-3 col-sm-3">


                    <div class="text-right">
                        <h2><?= round((100 / $totalTareas) * $tareasCompletadas, 2) ?>%</h2>
                    </div>

                </div>

                <div class="col-md-7 col-sm-7 h5">
                    <!--cosas por aqui...-->

                    <i class="fas fa-clock"><span><?= ' ' . $tiempo ?></span></i> 
                    <i class="fas fa-flag"><span><?= ' ' . $hitosCompletados . '/' . $totalHitos ?></span></i> 
                    <i class="fas fa-bullseye"><span><?= ' ' . $objetivosCompletados . '/' . $totalObjetivos; ?></span></i> 
                    <i class="fas fa-clipboard"><span><?= ' ' . $tareasCompletadas . '/' . $totalTareas; ?></span></i> 

                </div>

                <div class="col-md-5 col-sm-5">


                    <div class="text-right">    
                        <h3><i class="fas fa-calendar-alt"></i> <?= $fechaFix ?></h3>
                    </div>

                </div>


            </div>
        </div>

    </div>

    <?php
    $hitos = $model->hitos;
    if (count($hitos) === 0) {
        $hitos = 0;
        $hito = 0;
        $map = 0;
    } else {
        //evaluar y elegir el hito mas proximo a vencer
        $hitos = ArrayHelper::getColumn($model->hitos, 'id', 'nombre_descriptivo', 'fecha_limite');
        $hito = $hitos[0];
        $map = ArrayHelper::map($model->hitos, 'id', 'nombre_descriptivo');
    }


    echo Html::a('Continuar', ['proyectos/continuar', 'id' => $model->id, 'hito' => $hito], ['class' => 'd-none btn btn-zelda', 'id' => 'a-' . $model->id]);
    ?>

    <a onclick="clickSound('<?= $_SESSION['dir'] ?>', 'null', 'a-<?= $model->id ?>', 6541), startGame('<?= $_SESSION['dir'] ?>')" 
       class="d-none btn btn-zelda"
       id="<?= $model->id ?>">
        Continuar
    </a>
</div>