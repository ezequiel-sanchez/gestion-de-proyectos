<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Nuevo';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoProyecto(<?= $id ?>)
    });

//    onload dejo de funcionar al poner el ambiance en el onload del main >.<
//    al parecer este modo es mejor 
//    
//    onload = function omnom() {
//        vista('< ?= $_SESSION['vista'] ?>');
//    }

</script>

<div class="h-100 row text-center">

    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3 mt-n5">
                <!--                <div class="col-md-3 col-sm-3 mt-n5">
                                    <i class="fa fa-home display-4"></i>
                                </div>-->
            </div>
<!--                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'salir', 300)" 
                           class="a text-white display-4"
                           id="salir">
                            Salir
                        </a>-->
            <!--titulo-->
            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <div id="nuevo" class="op80 bgb text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Tiene nombre tu proyecto?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   class="btn btn-zelda" 
                                   onclick="mostrar('nuevo', 'nombre'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                                <!--< ?= Html::a('Si', ['proyectos/nuevo'], ['class' => 'btn btn-zelda']) ?>-->
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   class="btn btn-zelda" 
                                   onclick="generico('nuevo', 'plazo', 'nombre', 'Proyecto',<?= $cuenta ?>), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">Aún no</a>
                                <!--< ?= Html::a('No', ['proyectos/plazo'], ['class' => 'btn btn-zelda']) ?>-->
                            </div>

                        </div>
                    </div>
                </div>

                <div id="nombre" class="d-none bgb text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe el nombre de tu proyecto...</p>
                            <div class="row justify-content-center">
                                <div class="w-50 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'titulo_proyecto')->textInput(['maxlength' => true, 'placeholder' => 'Título del proyecto...', 'id' => 'nombre-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('nombre', 'plazo'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--plazo-->
                <div id="plazo" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Tiene plazo tu proyecto?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="mostrar('plazo', 'fecha'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="generico('plazo', 'tipo', 'fecha', 'Plazo'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">No</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="fecha" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="row justify-content-center">

                            <p class="px-5 shine text-white display-4">Selecciona el día que debes entregar tu proyecto.</p>

                            <div class="h-100 row justify-content-center field my-5 mb-n1 table table-striped">
                                <div class="caja bgb font-weight-bolder input-group-text">
                                    <?=
                                    $form->field($model, 'plazo_entrega')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INLINE,
                                        'options' => [
                                            'class' => 'd-none',
                                            'id' => 'fecha-campo'
                                        ],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                            'maxViewMode' => 2,
//                                    'autoclose' => true
                                        ]
                                    ])->label(false);
                                    ?>

                                </div>

                                <div class="w-100 row justify-content-center">
                                    <p id="fecha-error" class="w-50 invisible invalid-feedback d-block">error</p>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('fecha', 'tipo'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Siguiente</a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                            </div>
                        </div>


                    </div>
                </div>

                <!--tipo de proyecto-->
                <div id="tipo" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Selecciona que tipo de proyecto es...</p>
                            <div class="row justify-content-center">
                                <div class="w-50 field my-3">

                                    <select class="form-control" name="tipo" id="tipo-list" 
                                            onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                            >
                                        <option value="Estudios">Estudios</option>
                                        <option value="Investigación">Investigación</option>
                                        <option value="Personal">Personal</option>
                                    </select>

                                    <?= $form->field($model, 'tipo')->textInput(['class' => 'd-none', 'id' => 'tipo-campo'])->label(false) ?>

                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                            onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                            onclick="dropList('tipo', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_End', 'null', 0)">Siguiente</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="d-none text-center">
                    <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->
                    <?= $form->field($model, 'fecha_inicio')->textInput(['class' => 'd-none', 'id' => 'fechaInicial'])->label(false); ?>
                    <?= $form->field($model, 'fecha_fin')->textInput(['class' => 'd-none', 'id' => 'fechaFinal'])->label(false) ?>
                    <?= $form->field($model, 'id_user')->textInput(['class' => 'd-none', 'id' => 'userId'])->label(false) ?>
                    <?= $form->field($model, 'estado')->textInput(['class' => 'd-none', 'id' => 'estado'])->label(false) ?>

                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>