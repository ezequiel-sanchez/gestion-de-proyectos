<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Continuar';

$tiempo = 0;
$horas = 0;
$minutos = 0;
$segundos = 0;
$totalHitos = 0;
$hitosCompletados = 0;
$totalObjetivos = 0;
$objetivosCompletados = 0;
//$totalTareas = 0;
$tareasCompletadas = 0;
$completadosCount = 0;
$completadasCount = 0;
$tareasArray = $model->tareas;
$totalTareas = count($tareasArray);

foreach ($tareasArray as $tareaArray) {
    $completadasCount += $tareaArray->completada;
    $tareasCompletadas += $tareaArray->completada;
    if ($completadasCount == count($tareasArray)) {
        $objetivosCompletados++;
        $completadosCount++;
    }

    foreach (ArrayHelper::getColumn($tareaArray->sesiones, 'duracion') as $time) {
        $duracion = preg_split("/:/", $time);
        if (count($duracion) < 2) {
            $duracion = [0 => '00', 1 => '00', 2 => '00'];
        }
        $horas += intval($duracion[0]);
        $minutos += intval($duracion[1]);
        $segundos += intval($duracion[2]);
    }
}

$minutos += (int) ($segundos / 60);
$horas += (int) ($minutos / 60);
if ($horas < 10) {
    $horas = '0' . $horas;
}
$minutos -= 60 * (int) ($minutos / 60);
if ($minutos < 10) {
    $minutos = '0' . $minutos;
}
$tiempo = 'Total: ' . $horas . ':' . $minutos;

if ($totalTareas == 0) {
    $totalTareas = 1;
}
?>



<div class="bgb my-3 w-100" onmouseenter="hoverSound('<?= $_SESSION['dir'] ?>', 'Extra_Click')">        
    <div onclick="continuar(<?= $model->id ?>)" class="borde btn-zelda p-2">

        <div class="col align-self-lg-center">
            <div class="row justify-content-center">

                <div class="col-md-9 col-sm-9">
                    <h1 class="display-4"><?= $model->titulo ?></h1>
                </div>

                <div class="col-md-3 col-sm-3">


                    <div class="text-right">
                        <h2><?= round((100 / $totalTareas) * $tareasCompletadas, 2) ?>%</h2>
                    </div>

                </div>

                <div class="col-md-8 col-sm-8">
                    <p> <?= $model->descripcion ?></p>
<!--                    <p><?= $tiempo ?></p>
                    <p><?= $hitosCompletados . '/' . $totalHitos ?></p>
                    <p><?= $objetivosCompletados . '/' . $totalObjetivos; ?></p>
                    <p><?= $tareasCompletadas . '/' . $totalTareas; ?></p>
                    <p>< ?= ?></p>-->


                </div>

                <div class="col-md-4 col-sm-4">


                    <div class="text-right">    
                    </div>

                </div>


            </div>
        </div>

    </div>

    <a onclick="clickSound('<?= $_SESSION['dir'] ?>', 'HoverSmall_Click', 'a-<?= $model->id ?>', 663)" 
       class="d-none btn btn-zelda"
       id="<?= $model->id ?>">
        Continuar
    </a>

    <?= Html::a('Continuar', ['objetivos/continuar', 'id' => $model->id], ['class' => 'd-none btn btn-zelda', 'id' => 'a-' . $model->id]) ?>
</div>
