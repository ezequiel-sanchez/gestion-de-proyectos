<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <?php
            $config = ['template' => "{input}\n{error}\n{hint}"];
            $form = ActiveForm::begin();
            ?>

            <!--TITULO-->
            <div class="my-5 font-weight-bolder"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'titulo_proyecto')->textInput(['maxlength' => true, 'placeholder' => 'Título del proyecto...', 'id' => 'nombre-campo', 'class' => 'form-control text-center'])->label('Título del proyecto') ?>
            </div>

            <!--PLAZO-->
            <div class="h-75 field asdmt-3 asdmb-5 table table-striped">
                <div class="h-100 caja bgb font-weight-bolder input-group-text">

                    <?=
                    $form->field($model, 'plazo_entrega')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_INLINE,
                        'options' => [
                            'class' => 'd-none',
                            'id' => 'fecha-campo',
                        ],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'maxViewMode' => 2,
                        ]
                    ])->label('Plazo de entrega', ['class' => '']);
                    ?>

                </div>
            </div>

            <!--TIPO DE PROYECTO-->
            <div class="mt-5 mb-5">
                <?= $form->field($model, 'tipo')->textInput(['class' => 'd-none', 'id' => 'tipo-campo'])->label('Tipo de proyecto', ['class' => 'mb-n5']) ?>
                <select class="form-control text-center mt-n2" name="tipo" id="tipo-list"
                        onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                        >
                    <option value="Estudios">Estudios</option>
                    <option value="Investigación">Investigación</option>
                    <option value="Personal">Personal</option>
                </select>
            </div>

            <?= $form->field($model, 'estado')->textInput(['class' => 'd-none'])->label(false) ?>
            <?= $form->field($model, 'id_user')->textInput(['class' => 'd-none'])->label(false) ?>
            <?= $form->field($model, 'fecha_inicio')->textInput(['class' => 'd-none'])->label(false) ?>
            <?= $form->field($model, 'fecha_fin')->textInput(['class' => 'd-none'])->label(false) ?>

            <!--<div class="form-group">-->
            <!--< ?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>-->
            <!--    <div  class="row justify-content-center">
                    <div class="w-25 text-center  mb-5 pb-5">-->

            <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
               onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Close', 'sbtn', 663)" 
               class="bgb btn btn-zelda my-5"
               id="submit-button">
                Guardar
            </a>

            <?= Html::submitButton('Guardar', ['class' => 'd-none', 'name' => 'siguiente-button', 'id' => 'sbtn']) ?>
            <!--</div></div>-->
            <!--</div>-->

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>