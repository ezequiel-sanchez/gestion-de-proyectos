<?php

use yii\bootstrap4\Html;

$this->title = 'Plazo';
?>

<div class="h-100 row text-center">
    <div class="mt-5 pt-5 col align-self-lg-start">
        <div class="mt-5 pt-5 row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <div class="col-md-6 col-sm-6 text-center">

                <div class="">

                    <p class="shine text-white display-4">¿deadline?</p>

                </div>

                <div class="mt-5 row justify-content-center">

                    <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                        <?= Html::a('Si', ['proyectos/nuevo'], ['class' => 'btn btn-zelda']) ?>
                    </div>

                    <div class="col-md-1 col-sm-1">
                    </div>

                    <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                        <?= Html::a('No', ['proyectos/plazo'], ['class' => 'btn btn-zelda']) ?>
                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-3">
            </div>

        </div>
    </div>
</div>