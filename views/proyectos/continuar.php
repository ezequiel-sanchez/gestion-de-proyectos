<?php

use yii\bootstrap4\Html;
use yii\widgets\ListView;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;

$_SESSION['pagObjetivo'] = 0;
if (isset($_GET['page'])) {
    $_SESSION['pagObjetivo'] = $_GET['page'];
}

$this->title = 'Continuar';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        empty();
    });
</script>

<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
                   class="a fas fa-angle-left display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['proyectos/seleccionar', 'page' => $_SESSION['pagProyecto']], ['class' => 'd-none', 'id' => 'bbtn']) ?>
            </div> 
        </div> 

        <div class="col-md-1 col-sm-1 mt-7 ml-6 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Open', 'ebtn', 974)" 
                   class="a fa-solid fa-paintbrush display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['proyectos/editar', 'id' => $_SESSION['idProyecto']], ['class' => 'd-none', 'id' => 'ebtn']) ?>
            </div> 
        </div> 
    </div>
</div>

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <div class="col-md-6 col-sm-6">

                <div class="my-5 pb-5">
                    <!--<p></p>-->
                    <!--<h1 class="m-5 p-5 shine text-white display-4 text-center">< ?= $proyecto ?></h1>-->


                    <!--TABS-->
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">


                            <!--                        <li class="nav-item" role="presentation">
                                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" >Home</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" >Profile</a>
                                                    </li>
                                                    <li class="nav-item" role="presentation">
                                                        <a class="nav-link active" id="mas-tab" data-toggle="tab" href="#mas" role="tab" aria-controls="contact" >➕</a>
                                                    </li>-->

                            <?php
                            $i = 0;
                            $active = '';
//                            $href = '';
//                            $controls = '';
//                            $tab = '';
//                            for ($i = 0; $i < count($hitos); $i++) {
                            foreach ($hitos as $hito => $titulo) {
                                $i++;

//                                foreach ($valor as list($a, $b)){
//                                    echo "valor ->";
//                                echo "$b";
////                                echo "\$hitos[$valor] => $v.\n";
//                                echo "<- fin|  ";
//                                }
//                                                                    echo "valor ->";
//                                echo strval($key);
//                                echo " y ademas ";
//                                echo strval($val);
////                                echo "\$hitos[$valor] => $v.\n";
//                                echo "<- fin|  ";
//                                echo "valor ->";
//                                echo print_r($hitos[$valor]);
////                                echo "\$hitos[$valor] => $v.\n";
//                                echo "<- fin|  ";
                                if ($id == $hito) {
                                    $active = ' active';
                                } else {
                                    $active = '';
                                }
//                                $href = $valor[0];
//                                $controls = $valor[0];
//                                $tab = $valor[0];
//                                echo '<a class="nav-link'.$active.'" id="nav-'.$titulo.'-tab" data-toggle="tab" href="#'.$titulo.'" role="tab" aria-controls="nav-'.$titulo.'" >'.$titulo.'</a>';
                                echo


//                                 '
//                        . 'onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\')">'
//                        . ''.


                                '<div class="nav-link' . $active . '" onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'HoverBig\')" onclick="clickSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\', \'null\', 0)">' . // APERTURA DE CADA TAB
                                Html::a(// BOTON HITO
                                        $titulo . // txt-html  (boton hito) (previo al boton editar)
                                        Html::a(// BOTON EDITAR
                                                ' <div class="badge badge-zelda" onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'HoverSmall\')"><i class="fa-solid fa-paintbrush py-2px"></i></div>', // txt-html  (boton editar) 
                                                ['hitos/editar', 'id' => $hito], // ruta      (boton editar)
                                                ['class' => 'a' . $active]                                                      // css       (boton editar)
                                        ) . // FINAL DEL BOTON EDITAR
                                        '', // txt-html  (boton hito) (posterior al boton editar)
                                        ['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $hito], // ruta      (boton hito)
                                        ['class' => 'py-2 px-4 display-5 ml-n3 a text-white']                           // css       (boton hito)
                                );                                                                                  // FINAL DEL BOTON HITO
                                echo "</div>";                                                                          //CIERRE DE CADA TAB
                            }
                            ?>

                            <a onmouseenter="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                               onclick="clickSound('<?= $_SESSION['dir'] ?>', 'null', 'hitoNuevo', 0)" 
                               class="a text-white nav-link">
                                <i class="fas fa-plus"></i>
                            </a>
                            <?= Html::a('', ['hitos/nuevo'], ['class' => 'd-none', 'id' => 'hitoNuevo']) ?>
                        </div>
                    </nav>


                    <!--CONTENIDO-->
                    <!--                    <div class="tab-content" id="nav-tabContent">
                                            < ?php
                                            $ctab = '';
                                            for ($e = 0; $e < count($hitos); $e++) {
                                                $ctab = $hitos[$e];
                                                echo ''
                                                .'<div class="tab-pane fade" id="'.$ctab.'" role="tabpanel" aria-labelledby="'.$ctab.'-tab">'
                                                .'<p>holi: '.$e.'</p>'
                                                .'</div>';    
                                            }
                                            ?>
                                            <div class="tab-pane fade show" id="nav-mas" role="tabpanel" aria-labelledby="nav-mas-tab">
                                                <p>masmasmasmas</p>
                                            </div>
                                        </div>-->

                    <div class="pb-5">
                        <div class="pb-5">
                            <?=
                            ListView::widget([
                                "pager" => [
                                    "class" => \yii\bootstrap4\LinkPager::class
                                ],
                                "dataProvider" => $dp,
                                "itemView" => "_continuar",
                                "layout" =>
                                "{items}\n " .
//                                aqui podria controlar si hay 20 paneles no mostrar este elemento, poniendo un tag a los paneles y contandolos.
                                Html::a(''
                                        . ' <div class="bgb text-center" '
                                        . 'onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\')">'
                                        . '     <div class="borde btn-zelda">'
                                        . '         <i class="fas fa-plus"></i>'
                                        . '         <p class="display-5 mb-n1">Nuevo objetivo</p>'
                                        . '     </div>'
                                        . ' </div>'
                                        . '', ['objetivos/nuevo'], ['class' => 'a text-white text-center '])
                                . " \n{summary}\n{pager}",
                            ])
                            ?>

                        </div>
                    </div>




<!--<p>strval:   < ?= strval($ob) ?></p>-->
                </div>

                <!--                <div class="pb-5 scrollbarASDscrollbar-default">
                
                                    <div class="pb-5 ASDforce-overflow">
                                        < ?=
                                        ListView::widget([
                                            'pager' => [
                                                'class' => \yii\bootstrap4\LinkPager::class
                                            ],
                                            'dataProvider' => $dp[0],
                                            'itemView' => '_continuar',
                                            'layout' => "{items}\n{summary}\n{pager}",
                //                            'viewParams' => ['ob' => "$ob", 'type' => "$ type"],
                                        ]);
                                        ?>
                
                                    </div>
                
                                </div>-->

            </div>

            <div class="col-md-3 col-sm-3">
            </div>
            <div id="vacio" class="d-none">
                <?=
                Html::a(''
                        . ' <div class="bgb text-center" '
                        . 'onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\')">'
                        . '     <div class="borde btn-zelda">'
                        . '         <i class="fas fa-plus"></i>'
                        . '         <p class="display-5 mb-n1">Nuevo objetivo</p>'
                        . '     </div>'
                        . ' </div>'
                        . '', ['objetivos/nuevo'], ['class' => 'a text-white asdnav-link text-center '])
                ?>
            </div>
        </div>
    </div>
</div>