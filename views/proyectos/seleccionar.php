<?php

use yii\bootstrap4\Html;
use yii\widgets\ListView;

$_SESSION['pagProyecto'] = 0;
if (isset($_GET['page'])) {
    $_SESSION['pagProyecto'] = $_GET['page'];
}

$this->title = 'Seleccionar';
?>
<div class=" col-md-3 col-sm-3  mt-5 ml-5 fixed-top">
    <div class="col-md-1 col-sm-3  mt-5">
        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
           class="a fa fa-home display-4 mt-6 text-white">
        </a>
        <?= Html::a('', ['site/index'], ['class' => 'd-none', 'id' => 'bbtn']) ?>
    </div> 
</div> 

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <div class="col-md-6 col-sm-6">

                <div class="">
                    <h1 class="m-5 p-5 shine text-white display-4 text-center">Selecciona un proyecto</h1>
                </div>

                <div class="pb-5 scrollbarASDscrollbar-default">

                    <div class="pb-5 ASDforce-overflow">
                        <?=
                        ListView::widget([
                            'pager' => [
                                'class' => \yii\bootstrap4\LinkPager::class
                            ],
                            'dataProvider' => $dp,
                            'itemView' => '_seleccionar',
                            'layout' => "{items}\n{summary}\n{pager}",
//                            'viewParams' => ['award' => "$ award", 'type' => "$ type"],
                        ]);
                        ?>

                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-3">
            </div>

        </div>
    </div>
</div>