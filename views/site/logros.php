<?php

use yii\bootstrap4\Html;

$this->title = 'Logros';


//$nProyectos = 9999;
?>

<div class=" col-md-3 col-sm-3  mt-5 ml-5 fixed-top">
    <div class="col-md-1 col-sm-3  mt-5">
        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
           class="a fa fa-home display-4 mt-6 text-white">
        </a>
        <?= Html::a('', ['site/index'], ['class' => 'd-none', 'id' => 'bbtn']) ?>
    </div> 
</div> 

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-2 col-sm-2">

            </div>

            <div class="col-md-8 col-sm-8">

                <div class="">
                    <h1 class="m-5 p-5 shine text-white display-4 text-center">Estadísticas y logros</h1>
                </div>





                <div class="">
                    <div class="bgb my-3 w-100">        
                        <div class="borde p-2">
                            <div class="col align-self-lg-center">
                                <div class="row justify-content-center">

                                    <div class="col-md-12 col-sm-12 text-center">
                                        <h1 class="h1">Has empezado un total de <?= $nProyectos ?> proyectos.</h1>
                                        <h1 class="h1">Y has trabajado un total de <?= $horas ?> horas y <?= $minutos ?> minutos en ellos.</h1>

                                        <div class="my-5 row justify-content-center">

                                            <div class="shine col-md-3 col-sm-3 text-center">
                                                <div class="h-100 w-100 bgb my-2">        
                                                    <div class="h-100 w-100 borde p-2">
                                                        <div class = "p-5"> <h4>Has creado tu primer proyecto</h4> </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="shine am-2  col-md-3 col-sm-3 text-center">
                                                <div class="h-100 w-100 bgb my-2">        
                                                    <div class="h-100 w-100 borde p-2">
                                                        <div class = "p-5"> <h4>Has creado 10 proyectos</h4> </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <h1 class="display-4"> ¡Enhorabuena!</h1>

                                        <!--< ?= print_r($model->proyectos)?>-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>






                <div class="">
                    <h1 class="m-5 p-5 shine text-white display-4 text-center">Muchas gracias por tu esfuerzo y dedicación.</h1>
                </div>

            </div>

            <div class="col-md-2 col-sm-2">

            </div>

            <!--            <div>
                        < ?php
                        for ($i = 0; $i < 10; $i++) {
                            ?>
                            <div class="col-md-3 col-sm-3">
            
                            </div>
                            < ?php
                            for ($e = 1; $e < 7; $e++) {
                                ?>
                                <div class="col-md-1 col-sm-1">
                                    <div class="bgb">
                                        <div class="borde">
                                            <div onmouseover="hoverSound('< ?=$_SESSION['dir']?>','HoverSmall')" onclick="clickSound('< ?=$_SESSION['dir']?>','Extra_Click')" class="tal" id='sound'>
                                            < ?= $e ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                < ?php
                            }
                            ?>
                            <div class="col-md-3 col-sm-3">
            
                            </div>
                            < ?php
                        }
                        ?>
                        </div>-->

        </div>
    </div>
</div>