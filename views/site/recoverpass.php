<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\label\LabelInPlace;

$this->title = 'Recoverpass';
?>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        introAutoPlay();
    });
</script>
<div class="h-100 row text-center">
    <div class="col align-self-center">
        <div class="row justify-content-center">

            <div class="col-md-2 col-sm-2">
            </div>

            <div class="col align-self-center col-md-4 col-sm-4">
                <h3><?= $msg ?></h3>
            </div>

            <div class="col-md-2 col-sm-2 invisible">
                <div id="player"></div>
            </div>

            <div class="col-md-3 col-sm-3 op80 bgb">

                <div class="borde p-5">  

                    <div class="mb-5">
                        <h1>Recuperar contraseña</h1>
                    </div>

                    <?php
                    $config = ['template' => "{input}\n{error}\n{hint}"];
                    $form = ActiveForm::begin([
                                'method' => 'post',
                                'enableClientValidation' => true,
                    ]);
                    ?>

                    <div class="field my-3"
                         onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         > 
                             <?=
                             $form->field($model, 'email', $config)->widget(LabelInPlace::classname(), [
                                 'label' => 'Correo electronico...',
                                 'type' => LabelInPlace::TYPE_HTML5,
                                 'options' => ['type' => 'mail', 'class' => 'form-control']
                             ]);
                             ?>
                    </div>

                    <div class="my-5 bgb">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Sent', 'recuperar', 300)" 
                           class="btn btn-zelda">
                            Recuperar contraseña
                        </a>
                        <?= Html::submitButton('', ['class' => 'd-none', 'id' => 'recuperar', 'name' => 'recover-button']) ?>
                    </div>

                    <div class="my-5 bgb">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'volver', 300)" 
                           class="btn btn-zelda">
                            Volver
                        </a>
                        <?= Html::a('', ['site/login'], ['id' => 'volver']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>
    </div>
</div>