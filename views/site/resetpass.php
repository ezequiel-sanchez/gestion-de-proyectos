<?php
use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\label\LabelInPlace;
$this->title = 'Resetpass';
?>

<div class="h-100 row text-center">
    <div class="col align-self-center">
        <div class="row justify-content-center">

            <div class="col-md-2 col-sm-2">
            </div>

            <div class="col align-self-center col-md-4 col-sm-4">
                <h3><?= $msg ?></h3>
            </div>

            <div class="col-md-2 col-sm-2">
            </div>

            <div class="col-md-3 col-sm-3 op80 bgb">

                <div class="borde p-5">  

                    <div class="mb-5">
                        <h1>Restablecer contraseña</h1>
                    </div>

                    <?php
                    $config = ['template' => "{input}\n{error}\n{hint}"];
                    $form = ActiveForm::begin([
                                'method' => 'post',
                                'enableClientValidation' => true,
                    ]);
                    ?>

                    <div class="field my-3">
                        <?=
                        $form->field($model, 'email', $config)->widget(LabelInPlace::classname(), [
                            'label' => 'Correo electrónico...',
                            'type' => LabelInPlace::TYPE_HTML5,
                            'options' => ['type' => 'mail', 'class' => 'form-control']
                        ]);
                        ?>
                    </div>

                    <div class="field my-3">
                        <?=
                        $form->field($model, 'password', $config)->widget(LabelInPlace::classname(), [
                            'label' => 'Contraseña...',
                            'bsVersion' => '4.x',
                            'type' => LabelInPlace::TYPE_HTML5,
                            'options' => ['type' => 'password', 'class' => 'form-control']
                        ]);
                        ?>        
                    </div>

                    <div class="field my-3">
                        <?=
                        $form->field($model, 'password_repeat', $config)->widget(LabelInPlace::classname(), [
                            'label' => 'Repite contraseña...',
                            'bsVersion' => '4.x',
                            'type' => LabelInPlace::TYPE_HTML5,
                            'options' => ['type' => 'password', 'class' => 'form-control']
                        ]);
                        ?>        
                    </div>

                    <div class="field my-3">
                        <?=
                        $form->field($model, 'verificationCode', $config)->widget(LabelInPlace::classname(), [
                            'label' => 'Código de verificación...',
                            'type' => LabelInPlace::TYPE_HTML5,
                            'options' => ['type' => 'text', 'class' => 'form-control']
                        ]);
                        ?>
                    </div>
                    
                    <div class="form-group">
                        <?= $form->field($model, "recover")->input("hidden")->label(false) ?>  
                    </div>
                    
                    <div class="my-5 bgb">
                        <?= Html::submitButton('Restablecer', ['class' => 'btn btn-zelda', 'name' => 'register-button']) ?>
                    </div>

                    <div class="my-5 bgb">
                        <?= Html::a('Cancelar', ['site/login'], ['class' => 'btn btn-zelda']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>
    </div>
</div>