<?php

use yii\bootstrap4\Html;

$this->title = 'Main';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        particulasIndex();
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-end">
        <div class="row justify-content-center">

            <div class="col-md-8 col-sm-8">
                <div class="p-5">  
                </div>
            </div>

            <div class="mb-4 pb-4 col-md-3 col-sm-3 text-right user-select-none">

                <a href=""><div class="logo mb-5 mr-4"></div></a>

                <div class="py-5">

                    <div class="m-5 pr-5 py-1">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'null', 'comenzar', 6541), startGame('<?= $_SESSION['dir'] ?>')" 
                           class="a text-white display-4">
                            Comenzar
                        </a>
                        <?= Html::a('', ['site/comenzar'], ['id' => 'comenzar']) ?>
                    </div>

                    <div class="m-5 pr-5 py-1">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'HoverBig_Click', 'continuar', 1500)" 
                           class="a text-white display-4">
                            Continuar
                        </a>
                        <?= Html::a('', ['site/continuar'], ['id' => 'continuar']) ?>
                    </div>

                    <div class="m-5 pr-5 py-1">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Extra_Click', 'logros', 300)" 
                           class="a text-white display-4">
                            Logros
                        </a>
                        <?= Html::a('', ['site/logros'], ['id' => 'logros']) ?>
                    </div>

                    <div class="m-5 pr-5 py-1">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'salir', 300)" 
                           class="a text-white display-4">
                            Salir
                        </a>
                        <?= Html::a('', ['site/salir'], ['id' => 'salir']) ?>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>