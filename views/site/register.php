<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\label\LabelInPlace;

$this->title = 'Register';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
//        seleccionar();
        introAutoPlay();
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-center">
        <div class="row justify-content-center">

            <div class="col-md-2 col-sm-2 invisible">
                <div id="player"></div>
                <!--<button onclick="myFunction()">Click Me</button>-->
            </div>

            <div class="col align-self-center col-md-4 col-sm-4">
                <h3><?= $msg ?></h3>

                <!--                <h3 id="msg">asd</h3>
                                
                                <script>
                //                    document.write('<h1>'+'test'+'</h1>');
                //                    $asd = document.querySelector("body > div > div > main > div > div > div > div > div.col-md-3.col-sm-3.op80.bgb > div > div > h1").textContent;
                
                //                    var asd = "hols";
                //                    document.write('<h1>'+asd+'</h1>');
                                    
                //                    var e = document.querySelector("#formregister-username").autofocus();
                //                    var a= document.querySelector("body > div > div > main > div > div > div > div > div.col-md-3.col-sm-3.op80.bgb > div > div > h1");
                                    
                //                    document.write(a);
                                    
                //                    console.log(a);
                //                    console.log(e);
                
                //                    $asf = document.querySelector("body > div > div > main > div > div > div > div > div.col-md-3.col-sm-3.op80.bgb > div > div > h1").textContent;
                //                    document.write('<h1>'+$asf+'</h1>');
                
                //                    window.onclick = function(){     
                //                        var box = document.querySelector(".invalid-feedback");
                //                        console.log(box.textContent);
                //                        console.log(box.innerText);
                //                    };
                
                //                    console.log(box);
                                    
                //                    $asg = "fin";
                //                    document.write('<h1>'+$asg+'</h1>');
                var count =0;
                var estado =0;
                
                function myFunction() {
                  var x = document.getElementById("msg");
                  var y = document.querySelector(".invalid-feedback").textContent;
                  var z = "";
                  count++;
                  
                //  if (x.style.display === "none") 
                //  {
                //    x.style.display = "block";
                //  } 
                //  
                //  else 
                //  {
                //    x.style.display = "none";
                //  }
                
                  if (y === "")
                  {
                      z = " vacio ";
                  } 
                  else 
                  {
                      z = document.querySelector("#formulario > div:nth-child(1) > div > div.invalid-feedback").textContent;
                  }
                  
                  x.textContent= count+" "+z;
                }
                                    
                                </script>-->

            </div>

            <div class="col-md-2 col-sm-2">
            </div>

            <div class="col-md-3 col-sm-3 op80 bgb">

                <div class="borde p-5">  

                    <div class="mb-5">
                        <h1 id="titulo">Registro</h1>
                    </div>

                    <?php
                    $config = ['template' => "{input}\n{error}\n{hint}"];
                    $form = ActiveForm::begin([
                                'method' => 'post',
                                'id' => 'formulario',
                                'enableClientValidation' => false,
                                'enableAjaxValidation' => true,
                    ]);
                    ?>

                    <div class="field my-4"
                         onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         > 
                             <?=
                             $form->field($model, 'username', $config)->widget(LabelInPlace::classname(), [
                                 'label' => 'Nombre de usuario...',
                                 'type' => LabelInPlace::TYPE_HTML5,
                                 'options' => ['type' => 'text', 'class' => 'form-control']
                             ]);
                             ?>
                    </div>

                    <div class="field my-4"
                         onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         > 
                             <?=
                             $form->field($model, 'email', $config)->widget(LabelInPlace::classname(), [
                                 'label' => 'Correo electronico...',
                                 'type' => LabelInPlace::TYPE_HTML5,
                                 'options' => ['type' => 'mail', 'class' => 'form-control']
                             ]);
                             ?>
                    </div>

                    <div class="field my-4"
                         onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         > 
                             <?=
                             $form->field($model, 'password', $config)->widget(LabelInPlace::classname(), [
                                 'label' => 'Contraseña...',
                                 'bsVersion' => '4.x',
                                 'type' => LabelInPlace::TYPE_HTML5,
                                 'options' => ['type' => 'password', 'class' => 'form-control', 'autocomplete' => "new-password"]
                             ]);
                             ?>        
                    </div>

                    <div class="field my-4"
                         onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                         > 
                             <?=
                             $form->field($model, 'password_repeat', $config)->widget(LabelInPlace::classname(), [
                                 'label' => 'Repite contraseña...',
                                 'bsVersion' => '4.x',
                                 'type' => LabelInPlace::TYPE_HTML5,
                                 'options' => ['type' => 'password', 'class' => 'form-control', 'autocomplete' => "new-password"]
                             ]);
                             ?>
                    </div>

                    <div class="my-5 bgb">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Sent', 'registro', 300)" 
                           class="btn btn-zelda">
                            Registrar
                        </a>
                        <?= Html::submitButton('', ['class' => 'd-none', 'name' => 'register-button', 'id' => 'registro']) ?>
                    </div>

                    <div class="my-5 bgb">
                        <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                           onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'volver', 300)" 
                           class="btn btn-zelda">
                            Volver
                        </a>
                        <?= Html::a('', ['site/login'], ['id' => 'volver']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>
    </div>
</div>