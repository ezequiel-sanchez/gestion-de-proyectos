<?php
/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */

/** @var app\models\LoginForm $model */
//use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\label\LabelInPlace;

$this->title = 'Login';
?>
<script>
    
    document.addEventListener("DOMContentLoaded", function () {
        seleccionar();
        ajustarVolumen(2);
        introAutoPlay();
//        chispas();
    });
</script>
<div class="h-100 row text-center">
    <div class="h-100 col align-self-center">
        <div class="h-100 m-n5 tal justify-content-center <?= $_SESSION['presentacion'] ?>"><?= Html::a('<div class="m-5 p-5"><div class="m-5 p-5"><div class="m-5 p-5"><div class="m-5 p-5"><div class="m-5 p-0 bgb "> <div class="borde">Empezar </div></div></div></div></div></div>', ['site/crud'], ['class' => 'a text-white display-1']) ?></div>
        <div class="h-100 row justify-content-center <?= $_SESSION['iniciar'] ?>">

            <div onload="ajustarVolumen(2)" class="col-md-3 col-sm-3 invisible">
                <div id="player"></div>
                <!--<iframe src="https://www.youtube.com/embed/L6uXNrQZNQU?modestbranding=1&;showinfo=0&disablekb=1&autoplay=1" allow="autoplay"></iframe>-->
            </div>

            <div class="col-md-2 col-sm-2">
                <div class="h-25"> </div>
                <div id="particles-js-f1" class="h-50"> </div>
                <div id="particles-js-f2" class="h-25 mt-n6"> </div>
            </div>

            <div class="col-md-3 col-sm-3">
            </div>

            <div class="col-md-3 col-sm-3 col align-self-center">
                <div class="op80 bgb site-login">
                    <div class="borde">  

                        <a href=""><div class="logo mt-5 ml-2">
                            </div></a>

                        <div class="p-5">

                            <div class="mb-5">
                                <h1><?= Html::encode($this->title) ?></h1>
                            </div>

                            <?php
                            $config = ['template' => "{input}\n{error}\n{hint}"];
                            $form = ActiveForm::begin();
                            ?>

                            <div class="field my-3"
                                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                 > 
                                     <?=
                                     $form->field($model, 'usuario', $config)->widget(LabelInPlace::classname(), [
                                         'label' => 'Usuario...',
                                         'type' => LabelInPlace::TYPE_HTML5,
                                         'options' => ['type' => 'text', 'class' => 'form-control']
                                     ]);
                                     ?>
                            </div>

                            <div class="field my-3 pt-2"
                                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                 > 
                                     <?=
                                     $form->field($model, 'contraseña', $config)->widget(LabelInPlace::classname(), [
                                         'label' => 'Contraseña...',
                                         'bsVersion' => '4.x',
                                         'type' => LabelInPlace::TYPE_HTML5,
                                         //agregando la propiedad autocomplete "new-password", chrome no hace el autocompletado
                                         'options' => ['type' => 'password', 'class' => 'form-control', 'autocomplete' => "new-password"]
                                     ]);
                                     ?>    

                                <div class="mb-4 text-left">
                                    <?=
                                    $form->field($model, 'rememberMe')->checkbox([
                                        'label' => 'Recordarme',
                                    ])
                                    ?>
                                </div>

                            </div>

                            <div class="pt-2">
                                <div class="my-5 bgb">
                                    <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                       onclick="clickSound('<?= $_SESSION['dir'] ?>', 'HoverBig_Click', 'login', 900)" 
                                       class="btn btn-zelda"
                                       id="ux-login">
                                        Acceder
                                    </a>
                                    <?= Html::submitButton('Acceder', ['class' => 'd-none', 'name' => 'login-button', 'id' => 'login']) ?>
                                </div>

                                <div class="my-5 bgb">
                                    <div class="p-0">
                                        <a class="btn btn-zelda" 
                                           onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                           onclick="demo('<?= $_SESSION['dir'] ?>')">Demo</a>
                                    </div></div>

                                <div class="my-3">
                                    <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                                       onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Extra_Click', 'recoverpass', 300)" 
                                       class="a text-white text-center">
                                        ¿Olvidaste tu contraseña?
                                    </a>
                                    <?= Html::a('', ['site/recoverpass'], ['id' => 'recoverpass']) ?>
                                </div>

                                <div class="my-3">
                                    <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                                       onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Extra_Click', 'register', 300)" 
                                       class="a text-white text-center">
                                        ¿No tienes cuenta? Registrate
                                    </a>
                                    <?= Html::a('', ['site/register'], ['id' => 'register']) ?>
                                </div>

                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>