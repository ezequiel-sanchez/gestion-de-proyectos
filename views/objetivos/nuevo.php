<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Nuevo';
?>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        vista('<?= $_SESSION['vista'] ?>');
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
                <!--<p>el id seria: < ?= $id ?></p>-->
                <!--<p>la var submit es: < ?= $_SESSION['submit'] ?></p>-->
                <!--<p>la var id es: < ?= $_SESSION['idProyecto'] ?></p>-->
            </div>

            <!--titulo-->
            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <div id="nuevo" class="op80 bgb text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Quieres añadir un objetivo ahora?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a id="continuar" 
                                   class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="mostrar('nuevo', 'nombre'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <!--<a class="btn btn-zelda" onclick="generico('nuevo', 'submit', 'nombre', 'Objetivo')">Aún no</a>-->
                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'noAñadir', 663)" 
                                   class="btn btn-zelda">
                                    Aún no
                                </a>
                                <?= Html::a('', ['site/continuar'], ['id' => 'noAñadir']) ?>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="nombre" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe el título del objetivo...</p>
                            <div class="row justify-content-center">
                                <div class="w-50 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'placeholder' => 'Título del objetivo...', 'id' => 'nombre-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('nombre', 'descripcion'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--plazo-->
                <div id="descripcion" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Quieres añadirle una descripción a este objetivo ahora?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="mostrar('descripcion', 'texto'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="generico('descripcion', 'submit', 'texto', 'Descripción'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">No</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="texto" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe una descripción del objetivo...</p>
                            <div class="row justify-content-center">
                                <div class="w-75 field my-3"
                                    onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                    onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                    >
                                    <?= $form->field($model, 'descripcion')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Descripción del objetivo...', 'class' => 'dd-none', 'id' => 'texto-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center pt-5">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('texto', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_End', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="d-none text-center">
                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>