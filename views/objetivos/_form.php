<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="h-100 row">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <?php
            $config = ['template' => "{input}\n{error}\n{hint}"];
            $form = ActiveForm::begin();
            ?>

            <!--TITULO-->
            <div class="my-4 font-weight-bolder vista-mod-ancho"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'titulo')->textInput(['maxlength' => true, 'placeholder' => 'Título del proyecto...', 'id' => 'nombre-campo', 'class' => 'form-control text-center'])->label('Título del proyecto') ?>
            </div>

            <!--DESCRIPCION-->
            <div class="pb-5 font-weight-bolder field table table-striped"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'descripcion')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Descripción del objetivo...', 'class' => 'form-control text-justify', 'id' => 'texto-campo'])->label('Descripcion del objetivo') ?>
            </div>

            <div class="pt-4 pb-5"></div>
            <!--< ?= Html::submitButton('Guardar', ['class' => 'bgb btn btn-zelda my-5 w-50', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>-->

            <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
               onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Close', 'sbtn', 663)" 
               class="bgb btn btn-zelda my-5 w-50"
               id="submit-button">
                Guardar
            </a>
            <?= Html::submitButton('', ['class' => 'd-none', 'name' => 'siguiente-button', 'id' => 'sbtn']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>