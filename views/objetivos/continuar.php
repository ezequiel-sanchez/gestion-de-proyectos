<?php

use yii\bootstrap4\Html;
use yii\widgets\ListView;

$this->title = 'Continuar';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        empty();
    });
</script>


<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
                   class="a fas fa-angle-left display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']], ['class' => 'd-none', 'id' => 'bbtn']) ?>
            </div> 
        </div> 

        <div class="col-md-1 col-sm-1 mt-7 ml-6 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Open', 'ebtn', 974)" 
                   class="a fa-solid fa-paintbrush display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['objetivos/editar', 'id' => $_SESSION['idObjetivo']], ['class' => 'd-none', 'id' => 'ebtn']) ?>
            </div> 
        </div> 
    </div>
</div>


<!--ESTE ES EL BUENO AHORA-->
<!--<div class=" col-md-2 col-sm-2 mt-7 ml-5 fixed-top">
    <div class="col-md-1 col-sm-3">

< ?= Html::a('', ['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']], ['class' => 'a fas fa-angle-left display-3 mt-5 text-white text-center']) ?>
    </div> 
</div> -->

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <div class="col-md-6 col-sm-6">

                <div class="my-5 pb-5">
                    <h1 class="shine text-white display-4 text-center">Objetivo: <?= ucfirst($titulo) ?></h1>

                    <!--RESULTADOS ESPERADOS-->
                    <div class="mt-5 bar">
                        <p class="bar">Productos esperados para este objetivo</p>

                        <?php
                        $i = 0;
                        foreach ($productos as $producto) {
                            $i++;
                            echo '<p><i class="fa-solid fa-caret-right"></i> ' . $producto . '</p>';
                        }
                        ?>
                    </div>

                    <!--TAREAS-->
                    <div class="pb-5">
                        <div class="pb-5">
                            <?=
                            ListView::widget([
                                "pager" => [
                                    "class" => \yii\bootstrap4\LinkPager::class
                                ],
                                "dataProvider" => $dp,
                                "itemView" => "_continuar",
                                "layout" =>
                                "{items}\n " .
//                                aqui podria controlar si hay 20 paneles no mostrar este elemento, poniendo un tag a los paneles y contandolos.
                                Html::a(''
                                        . ' <div class="bgb text-center" '
                                        . 'onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\')">'
                                        . '     <div class="borde btn-zelda">'
                                        . '         <i class="fas fa-plus"></i>'
                                        . '         <p class="display-5 mb-n1">Nueva tarea</p>'
                                        . '     </div>'
                                        . ' </div>'
                                        . '', ['tareas/nuevo'], ['class' => 'a text-white text-center '])
                                . " \n{summary}\n{pager}",
                            ])
                            ?>

                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-3 col-sm-3">
            </div>
            <div id="vacio" class="d-none">
                <?=
                Html::a(''
                        . ' <div class="bgb text-center" '
                        . 'onmouseenter="hoverSound(\'' . $_SESSION['dir'] . '\', \'Extra_Click\')">'
                        . '     <div class="borde btn-zelda">'
                        . '         <i class="fas fa-plus"></i>'
                        . '         <p class="display-5 mb-n1">Nueva tarea</p>'
                        . '     </div>'
                        . ' </div>'
                        . '', ['tareas/nuevo'], ['class' => 'a text-white text-center '])
                ?>
            </div>
        </div>
    </div>
</div>