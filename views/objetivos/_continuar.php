<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Continuar';
if ($model->completada === 1) {
    $completada = 'fa-solid fa-check';
} else {
    $completada = '';
}
$tiempo = 0;
$tal = 0;
$horas = 0;
$minutos = 0;
$segundos = 0;
$sesiones = count($model->sesiones);

//print_r($model->sesiones);
foreach (ArrayHelper::getColumn($model->sesiones, 'duracion') as $time) {
    $duracion = preg_split("/:/", $time);
    if (count($duracion) < 2) {
        $duracion = [0 => '00', 1 => '00', 2 => '00'];
    }
    $horas += intval($duracion[0]);
    $minutos += intval($duracion[1]);
    $segundos += intval($duracion[2]);
}
//                                            $sum = $segundos/60;
$minutos += (int) ($segundos / 60);
$horas += (int) ($minutos / 60);
$minutos -= 60 * (int) ($minutos / 60);
$tal = 'horas: ' . $horas . ' minutos: ' . $minutos/* . ' segundos: '.$segundos */;
//                                                echo '<p>- ' . $tal . '</p>';

$_SESSION['tiempo-' . $model->id] = '<h4>Tiempo invertido</h4><h4>Horas: ' . $horas . ' Minutos: ' . $minutos . '</h4>';
?>

<div class="bgb my-3 w-100" onmouseenter="hoverSound('<?= $_SESSION['dir'] ?>', 'Extra_Click')">        
    <div onclick="continuar(<?= $model->id ?>)" class="borde btn-zelda p-2">

        <div class="col align-self-lg-center">
            <div class="row justify-content-center">

                <div class="col-md-9 col-sm-9">
                    <h1 class="display-4"><i class="<?= $completada ?>"></i> <?= $model->nombre ?></h1>
                </div>

                <div class="col-md-3 col-sm-3">

                    <div class="text-right">
                        <h3>Sesiones: <?= $sesiones ?></h3>
                    </div>

                </div>

                <div class="col-md-8 col-sm-8 text-justify">
                    <p> <?= $model->descripcion ?></p>
                </div>

                <div class="col-md-4 col-sm-4 align-self-lg-end">

                    <div class="text-right">    
                        <h4>Tiempo invertido</h4>
                        <h4><?= ' Horas: ' . $horas . ' Minutos: ' . $minutos ?></h4>
                    </div>

                </div>

            </div>
        </div>

    </div>

    <a onclick="clickSound('<?= $_SESSION['dir'] ?>', 'HoverSmall_Click', 'a-<?= $model->id ?>', 663)" 
       class="d-none btn btn-zelda"
       id="<?= $model->id ?>">
        Continuar
    </a>

    <?= Html::a('Continuar', ['tareas/continuar', 'id' => $model->id], ['class' => 'd-none btn btn-zelda', 'id' => 'a-' . $model->id]) ?>
</div>