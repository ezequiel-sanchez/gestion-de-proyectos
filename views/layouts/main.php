<?php
/** @var yii\web\View $this */

/** @var string $content */
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use app\assets\FontAsset;

FontAsset::register($this);
AppAsset::register($this);
//if (isset($_SESSION['vista'])) {
//    $_SESSION['test1'] = $_SESSION['vista'];
//}
$_SESSION['vista'] = Yii::$app->getView()->title;

if (Yii::$app->getView()->title == 'Main') {
    $gradient = "capa-1";
} else {
    $gradient = "capa-2";
}

//$js = <<< JS
//
// krajeeDialog.confirm = function (message, callback) {
//    swal({
//
//title: message,
//
//type: "warning",
//showCancelButton: true,
//confirmButtonColor: "#5f022a",
//confirmButtonText: "Continuar",
//cancelButtonText: "Cancelar",
//closeOnConfirm: false,
//closeOnCancel: true,
//        title: message,
//        type: "warning",
//        showCancelButton: true,
//        closeOnConfirm: true,
//        allowOutsideClick: true
//    }, callback);
//}
//JS;
//$this->registerJs($js, yii\web\view::POS_READY);

$homeUrl = Yii::$app->homeUrl;
$_SESSION['dir'] = substr($homeUrl, 1, -14);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>" class="h-100 user-select-none">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v6.1.1/css/all.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="shortcut icon" href="<?= Yii::$app->request->baseUrl ?>/images/icono-logo.png" type="image/x-icon" /> 
        <?php $_SESSION['dir'] = substr($homeUrl, 1, -14); ?>
        <!--        < ?=
                $js = <<<SCRIPT
                $(function () {
                            $("[data-toggle='tooltip']") . tooltip();
                        });
                ;
        
                $(function () {
                            $("[data-toggle='popover']") . popover();
                        });
                SCRIPT;
        
                $this->registerJs($js);
                ? >-->

    </head>
    <body class="w-100 d-flex flex-column h-100 background">
        <div class="w-100 d-flex flex-column h-100">
            <div class="w-100 d-flex flex-column h-100 <?= $gradient ?>">
                <?php $this->beginBody() ?>
                <!-- particles.js container --> 
                <div id="particles-js"></div> 
                <!-- stats - count particles --> 
                <!--<div class="count-particles"> <span class="js-count-particles">--</span> particles </div>--> 
                <!-- particles.js lib - https://github.com/VincentGarreau/particles.js --> 
                <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script> 
                <!-- stats.js lib --> 
<!--                <script src="http://threejs.org/examples/js/libs/stats.min.js"></script>-->

                <header>
                    <?php
                    if (!Yii::$app->user->isGuest) {
//                        if (!in_array(Yii::$app->view->title, ['Main', 'Comenzar', 'Continuar', 'Logros'], true)) {
                        if ($_SESSION['layout']) {
//                            NavBar::begin([
//                                'brandLabel' => Yii::$app->name,
//                                'brandUrl' => Yii::$app->homeUrl,
//                                'options' => [
//                                    'class' => 'navbar navbar-expand-md ASDnavbar-dark ASDbg-dark fixed-top',
//                                ],
//                            ]);
//                            echo Nav::widget([
//                                'options' => ['class' => 'navbar-nav'],
//                                'items' => [
//                                    ['label' => 'tal'],
//                                    ['label' => 'Home', 'url' => ['/site/index']],
//                                    ['label' => 'CRUD', 'url' => ['/site/crud']],
//                                    ['label' => 'Contact', 'url' => ['/site/contact']],
////                                    Yii::$app->user->isGuest ? (
////                                            ['label' => 'Login', 'url' => ['/site/login']]
////                                            ) : (
////                                            '<li>'
////                                            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
////                                            . Html::submitButton(
////                                                    'Logout (' . Yii::$app->user->identity->username . ')',
////                                                    ['class' => 'btn btn-link logout']
////                                            )
////                                            . Html::endForm()
////                                            . '</li>'
////                                            )
//                                ],
//                            ]);
//                            NavBar::end();
//                            echo '<div class="navbar-bg"><div class="navbar">hola</div></div>';
                            $_SESSION['ruta'] = Yii::$app->controller->getRoute();
                            $_SESSION['pos1'] = '';
                            $_SESSION['pos2'] = '';
                            $_SESSION['pos3'] = '';
                            $_SESSION['pos4'] = '';
                            $_SESSION['pos5'] = '';
                            $_SESSION['d4'] = '';
                            $_SESSION['dd4'] = '';
                            $_SESSION['ddd4'] = '';
                            $_SESSION['d5'] = '';
                            switch ($_SESSION['ruta']) {
                                case '1':
                                    $_SESSION['d5'] = '';
                                    break;
                                case '2':
                                    $_SESSION['d5'] = '';
                                    break;
                                case 'proyectos/continuar':
                                case 'proyectos/editar':
                                    $_SESSION['pos3'] = 'shine fa-brands fa-ethereum';
                                    $_SESSION['pos4'] = '';
                                    $_SESSION['pos5'] = '';
                                    $_SESSION['dd4'] = 'd-none';
                                    $_SESSION['ddd4'] = 'fa-solid fa-question';
                                    $_SESSION['d4'] = 'fa-solid fa-question';
                                    $_SESSION['d5'] = 'fa-solid fa-question';
                                    break;
                                case 'objetivos/continuar':
                                case 'objetivos/editar':
                                    $_SESSION['pos3'] = '';
                                    $_SESSION['pos4'] = 'shine fa-brands fa-ethereum';
                                    $_SESSION['pos5'] = '';
                                    $_SESSION['d4'] = 'fa-solid fa-cubes-stacked';
                                    $_SESSION['dd4'] = '';
                                    $_SESSION['d5'] = 'fa-solid fa-question';
                                    break;
                                case 'tareas/continuar':
                                case 'tareas/editar':
                                    $_SESSION['pos3'] = '';
                                    $_SESSION['pos4'] = '';
                                    $_SESSION['pos5'] = 'shine fa-brands fa-ethereum';
                                    $_SESSION['d4'] = 'fa-solid fa-cubes-stacked';
                                    $_SESSION['dd4'] = '';
                                    $_SESSION['d5'] = 'fa-solid fa-dragon';
                                    break;
                                default:
//                                    $_SESSION['tipo'] = '<i class="fa-solid fa-seedling"></i> ';
                            }

                            echo ''
                            .
                            '<div class="navbar-bg">
        <div class="col align-self-lg-center navbar">

            <div class="w-100 row justify-content-center">


                <div class="col-md-4 col-sm-4 col align-self-lg-center">

                    <div class="col-md-12 col-sm-12">
                        <h2 class="text-center"> ' . strval($_SESSION['tipo']) . ucfirst($_SESSION['proyecto']) . ' </h2>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <h2 class="text-white text-center">' . '<i class="fas fa-calendar-alt"></i> ' . $_SESSION['plazo'] . ' <i class="fas fa-clock"></i> ' . $_SESSION['tiempo'] . '</h2>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <h2 class="text-white text-center">' . '<i class="fas fa-flag"></i> ' . $_SESSION['totalHitos'] . ' <i class="fas fa-bullseye"></i> ' . $_SESSION['totalObjetivos'] . ' <i class="fas fa-clipboard"></i> ' . $_SESSION['totalTareas'] . '</h2>
                    </div>
                </div>

<!--MAPA-->
                <div id="mapa" class="col-md-5 d-flex col-sm-5 col align-self-lg-center text-center p-2 map">
                    <!--style="background-color: yellow">-->
                    <div id="zona-1" class="col align-self-lg-center text-center">
                         <!--style="background-color: blue">-->
                         <h1 onclick="clickSound(\'' . $_SESSION['dir'] . '\', \'MapClick\', \'z1\', 7000)" onmouseover="hoverSound(\'' . $_SESSION['dir'] . '\', \'MapHover\')" class="a text-white fa-solid fa-dungeon"></h1>
                         ' . Html::a('', ['site/index'], ['class' => 'd-none', 'id' => 'z1']) . ' 
                    </div>

                    <div id="zona-2" class="col align-self-lg-center text-center">
                        <!--style="background-color: blue">-->
                        <h1 onclick="clickSound(\'' . $_SESSION['dir'] . '\', \'MapClick\', \'z2\', 7000)" onmouseover="hoverSound(\'' . $_SESSION['dir'] . '\', \'MapHover\')" class="a text-white fa-brands fa-fort-awesome"></h1>
                         ' . Html::a('', ['proyectos/seleccionar', 'page' => $_SESSION['pagProyecto']], ['class' => 'd-none', 'id' => 'z2']) . ' 
                    </div>

                    <div id="zona-3" class="col align-self-lg-center text-center">
                        <!--style="background-color: blue">-->
                        <i class="' . $_SESSION['pos3'] . '"></i>
                            <h1 onclick="clickSound(\'' . $_SESSION['dir'] . '\', \'MapClick\', \'z3\', 7000)" onmouseover="hoverSound(\'' . $_SESSION['dir'] . '\', \'MapHover\')" class="fa-solid fa-chess-rook a text-white"></h1>
                        ' . Html::a('', ['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']], ['class' => 'd-none', 'id' => "z3"]) . '  
                    </div>

                    <div id="zona-4" class="col align-self-lg-center text-center">
                        <!--style="background-color: blue">-->
                        <h1 class="' . $_SESSION['ddd4'] . '"></h1>
                        <i class="' . $_SESSION['pos4'] . '"></i>
                            <h1 onclick="clickSound(\'' . $_SESSION['dir'] . '\', \'MapClick\', \'z4\', 7000)" onmouseover="hoverSound(\'' . $_SESSION['dir'] . '\', \'MapHover\')" class="a text-white ' . $_SESSION['d4'] . ' ' . $_SESSION['dd4'] . '"></h1>
                         ' . Html::a('', ['objetivos/continuar', 'id' => $_SESSION['idObjetivo'], 'hito' => $_SESSION['idHito']], ['class' => 'd-none', 'id' => 'z4']) . ' 
                    </div>

                    <div id="zona-5" class="col align-self-lg-center text-center">
                        <!--style="background-color: blue">-->
                        <i class="' . $_SESSION['pos5'] . '"></i>
                        <h1 class="' . $_SESSION['d5'] . '"> </h1>
                    </div>
                </div>
                <!--MAPA fin-->


                <div class="col-md-3 col-sm-3 col align-self-lg-center text-right">

                    <div class="h-50 col-md-12 col-sm-12">
                        <h1>Notificaciones <i class="a fas fa-bell"></i></h1>
                    </div>

                    <div class="h-50 col-md-12 col-sm-12">
                        <h1>Recordatorios <i class="a fas fa-calendar-plus"></i></h1>
                    </div>
                </div>
            </div>
        </div>
    </div>'
                            . '';
                        }
                    } else {
//        if ( Url::toRoute( Url::home() ) == Url::toRoute( Yii::$app->controller->getRoute() ) ) {
//	// it is Home page
//            echo "si";
//}
                        if (!in_array(Yii::$app->view->title, ['Login', 'Recoverpass', 'Register', 'Resetpass'], true)) {
//        return $this->redirect(["site/login"]);
//        echo "bla bla bla, redireccionando ...<br/>";
                            Yii::$app->getResponse()->redirect(['site/login'])->send();
                            return;
                        }
                    }

//                        if (in_array(Yii::$app->view->title, ['Seleccionar', 'otras'], true)) {
//                            echo 
//                            '
//                                <div class=" col-md-3 col-sm-3  mt-5 ml-5 fixed-top">
//                                    <div class="col-md-1 col-sm-3  mt-5">
//                                    
//                                        '.Html::a(''
//                                                . ''
//                                                . ''
//                                                . ''
//                                                . ''
//                                                . ''
//                                                . ''
//                                                . ''
//                                                . '', ['site/index'], ['class' => 'a fa fa-home display-4 text-white']).'
//                                    </div> 
//                                </div>  
//                            ';
//                        }
                    ?>
                </header>

                <main role="main" class="h-100">
                    <div class="container-fluid h-100 force-overflow">
                        <?=
                        Breadcrumbs::widget([
                            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                </main>


                <footer class="d-none container-fluid footer mt-auto py-3 text-muted" hidden> 
                    <!--<footer class="container-fluid footer mb-5 mt-n6 asdpy-3 text-muted">-->
                        <!--<script src="../js/jquery.js"></script>-->
            <!--            <script>
                $("#buscador").on("keyup", function () {
            
                    var value = $(this).val().toLowerCase();
            //  if(!$.isNumeric(value)){
                    $(".table tbody tr").filter(function () {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value); > -1)
                    });
            //    }
                });
            
            </script>-->
                    <?=
                    $asd = "";
//                    echo "ID de session: ";
//                    echo Yii::$app->session->id;
//                    echo "<br/>getRoute: ";
//                    echo Yii::$app->controller->getRoute();
//                    echo "<br/>app->viewpath: ";
//                    echo Yii::$app->viewPath;
//
//                    echo "<br/>app->view->title: ";
//                    echo Yii::$app->view->title;
//                    echo "<br/>app->state ";
//                    echo Yii::$app->state;
//                    echo "<br/>app->getUrlManager()->baseUrl: ";
//                    echo Yii::$app->getUrlManager()->baseUrl;
//                    echo "<br/>app->getSession()->count: ";
//                    echo Yii::$app->getSession()->count;
//                    echo "<br/>app->getView()->title: ";
//                    echo Yii::$app->getView()->title;
//
//                    echo "<br/>app->getUser()->isGuest ";
//                    echo Yii::$app->getUser()->isGuest;
//            echo "<br/>- vari 1: ";
//if (isset($_SESSION['test1'])){
////$_SESSION['test1'];
////static $vari1 = $vari2;
//    VarDumper::dump($_SESSION['test1']);
//}
//            
//            echo "<br/>- vari 2: ";
//            echo $_SESSION['vista'];
                    ?>


                    <div class="container">
                        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
                        <p class="float-right"><?= Yii::powered() ?></p>
                    </div>
                </footer>

                <?php $this->endBody() ?>

            </div></div></body>
</html>
<?php $this->endPage() ?>


<script>
//    updateParticulas();
//    warp();
<?php
$_SESSION['vista'] = Yii::$app->getView()->title;
$_SESSION['dir'] = substr($homeUrl, 1, -14);
if (!isset($_SESSION['layout'])) {
    $_SESSION['layout'] = false;
}
?>
//    debug('< ?= strval($_SESSION['vista']); ?>');
    document.addEventListener("DOMContentLoaded", function () {
        ambience('<?= $_SESSION['dir'] ?>', '<?= $_SESSION['layout'] ?>');
    });

//    document.body.addEventListener("mousemove", function () {
////        unMute();
//    });
</script>