<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Nuevo';
?>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoOEH(<?= $_SESSION['idObjetivo'] ?>, <?= $_SESSION['idHito'] ?>);
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <!--titulo-->
            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="text-center">

                    <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->
                    <?= $form->field($model, 'id_objetivo')->textInput(['class' => 'd-none', 'id' => 'objetivoId'])->label(false) ?>
                    <?= $form->field($model, 'id_hito')->textInput(['class' => 'd-none', 'id' => 'hitoId'])->label(false) ?>

                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>
