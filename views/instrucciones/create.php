<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrucciones */

$this->title = 'Create Instrucciones';
$this->params['breadcrumbs'][] = ['label' => 'Instrucciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrucciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
