<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="h-100 row">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <?php
            $config = ['template' => "{input}\n{error}\n{hint}"];
            $form = ActiveForm::begin();
            ?>

            <?= $form->field($model, 'id_tarea')->textInput(['class' => 'd-none'])->label(false) ?>

            <!--DESCRIPCION-->
            <div class="pb-5 mb-5 font-weight-bolder field vista-mod-ancho"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'paso')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Descripción del paso a realizar...', 'class' => 'form-control text-justify', 'id' => 'texto-campo'])->label('Descripción del paso a realizar', ['class' => 'mb-3']) ?>
            </div>

            <div class="pt-5 pb-5"></div>

            <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
               onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Close', 'sbtn', 663)" 
               class="bgb btn btn-zelda my-5 w-50"
               id="submit-button">
                Guardar
            </a>
            <?= Html::submitButton('', ['class' => 'd-none', 'name' => 'siguiente-button', 'id' => 'sbtn']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<!--<div class="instrucciones-form">

< ?php $form = ActiveForm::begin(); ?>

< ?= $form->field($model, 'id_tarea')->textInput() ?>

< ?= $form->field($model, 'paso')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
< ?= Html::submitButton('Guardar', ['class' => 'bgb btn btn-zelda my-5 w-50', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>
    </div>

< ?php ActiveForm::end(); ?>

</div>-->
