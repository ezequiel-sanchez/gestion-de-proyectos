<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;

$this->title = 'Nuevo';
$msg = "¿Quieres añadirle una instrucción ahora a tu tarea?";
if (intval($_SESSION['instruccionesCount']) > 0) {
    $msg = "¿Quieres seguir agregando instrucciones a esta tarea ahora?";
}
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoInstrucciones(<?= $_SESSION['idTarea'] ?>);
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>

            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <!--¿CONTINUAR?-->
                <div id="nuevo" class="op80 bgb text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4"><?= $msg ?></p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="mostrar('nuevo', 'instruccion')">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'noAñadir', 663)" 
                                   class="btn btn-zelda">
                                    No
                                </a>
                                <?= Html::a('', ['tareas/continuar', 'id' => $_SESSION['idTarea']], ['id' => 'noAñadir']) ?>
                            </div>

                        </div>
                    </div>
                </div>

                <!--INSTRUCCIÓN-->
                <div id="instruccion" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe la instrucción...</p>
                            <div class="row justify-content-center">
                                <div class="w-75 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'paso')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Instrucción de la tarea...', 'class' => 'dd-none', 'id' => 'instruccion-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center pt-5">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('instruccion', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_End', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="d-none text-center">

                    <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->
                    <?= $form->field($model, 'id_tarea')->textInput(['class' => 'd-none', 'id' => 'tareaId'])->label(false) ?>

                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>