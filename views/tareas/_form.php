<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
use yii\widgets\LinkPager;

/* @var $this yii\web\View */
/* @var $model app\models\Tareas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="h-100 row">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <?php
            $config = ['template' => "{input}\n{error}\n{hint}"];
            $form = ActiveForm::begin();
            ?>

            <div class="my-4 font-weight-bolder vista-mod-ancho"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
            </div>

            <div class="pb-5 font-weight-bolder field table table-striped"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?= $form->field($model, 'descripcion')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Descripción de la tarea...', 'class' => 'form-control text-justify', 'id' => 'texto-campo'])->label('Descripcion de la tarea') ?>
            </div>
            <div class="pt-4 pb-5"></div>

            <?= $form->field($model, 'completada')->textInput(['class' => 'd-none'])->label(false) ?>
            <?= $form->field($model, 'id_objetivo')->textInput(['class' => 'd-none'])->label(false) ?>

            <div class="form-group">

                <!--< ?= Html::submitButton('Guardar', ['class' => 'bgb btn btn-zelda my-5 w-50', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>-->
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Close', 'sbtn', 663)" 
                   class="bgb btn btn-zelda my-5 w-50"
                   id="submit-button">
                    Guardar
                </a>
                <?= Html::submitButton('', ['class' => 'd-none', 'name' => 'siguiente-button', 'id' => 'sbtn']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
