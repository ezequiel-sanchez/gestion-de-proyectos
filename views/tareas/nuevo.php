<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;

$this->title = 'Nuevo';
?>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoTarea(<?= $_SESSION['idObjetivo'] ?>);
        clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0);
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
            </div>


            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <!--nombre-->
                <div id="nombre" class="bgb text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe como se llama la tarea...</p>
                            <div class="row justify-content-center">
                                <div class="w-50 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'nombre')->textInput(['maxlength' => true, 'placeholder' => 'Título de la tarea...', 'id' => 'nombre-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('nombre', 'mensaje'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--descripcion-->
                <div id="mensaje" class="d-none op80 bgb text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Quieres describir la tarea ahora?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="mostrar('mensaje', 'descripcion'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                   onclick="mostrar('mensaje', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">Aún no</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="descripcion" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Describe la tarea a realizar...</p>
                            <div class="row justify-content-center">
                                <div class="w-75 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'descripcion')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Descripción de la tarea...', 'class' => 'dd-none', 'id' => 'descripcion-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center pt-5">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')"
                                       onclick="siguiente('descripcion', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_End', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="d-none text-center">

                    <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->
                    <?= $form->field($model, 'completada')->textInput(['class' => 'd-none', 'id' => 'estado'])->label(false) ?>
                    <?= $form->field($model, 'id_objetivo')->textInput(['class' => 'd-none', 'id' => 'objetivoId'])->label(false) ?>

                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>