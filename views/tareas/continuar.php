<?php

use yii\bootstrap4\Html;
use yii\widgets\ListView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Continuar';
?>

<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
                   class="a fas fa-angle-left display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['objetivos/continuar', 'id' => $_SESSION['idObjetivo'], 'hito' => $_SESSION['idHito']], ['class' => 'd-none', 'id' => 'bbtn']) ?>
            </div> 
        </div> 

        <div class="col-md-1 col-sm-1 mt-7 ml-6 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Open', 'ebtn', 974)" 
                   class="a fa-solid fa-paintbrush display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['tareas/editar', 'id' => $_SESSION['idTarea']], ['class' => 'd-none', 'id' => 'ebtn']) ?>
            </div> 
        </div> 
    </div>
</div>



<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-2 col-sm-2">
            </div>

            <div class="col-md-8 col-sm-8">

                <div class="my-5 pb-5">

                    <div class="pb-5">
                        <div class="pb-5">
                            <?=
                            ListView::widget([
                                "pager" => [
                                    "class" => \yii\bootstrap4\LinkPager::class
                                ],
                                "dataProvider" => $dp,
                                "itemView" => "_continuar",
                                "layout" => "{items}",
                                "viewParams" => ['ls' => $ls],
                            ])
                            ?>

                        </div>
                    </div>

                </div>

            </div>

            <div class="col-md-2 col-sm-2">
            </div>

        </div>
    </div>
</div>