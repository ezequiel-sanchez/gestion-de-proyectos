<?php

use yii\bootstrap4\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Continuar';
$estado = '';
$mostrar = '';
$mostrarTiempo = '';

if (!isset($_SESSION['idSesion'])) {
    $_SESSION['idSesion'] = '';
}

if ($model->completada === 0) {
    $completada = 'completada';
    $mostrar = '';
    $mostrarTiempo = 'd-none ';
} else {
    $completada = 'incompleta';
    $estado = 'fa-solid fa-check';
    $mostrar = 'd-none ';
    $mostrarTiempo = '';
}
$contador = 'patata';
$trabajar = '';
$parar = 'd-none ';
$trabajando = 0;

if (isset($_SESSION['finSesion'])) {
    $contador = '00:00:00';
} else {
    if (isset($_SESSION['inicioSesion'])) {
        $contador = substr($_SESSION['inicioSesion'], 11, 8);
        $trabajando = 1;
        $trabajar = 'd-none ';
        $parar = '';
    }
}
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        init(<?= $trabajando ?>, '<?= $contador ?>');
        fin(<?= $trabajando ?>, '<?= $estado ?>');
        ajustarVolumen(13);
    });
</script>

<div class="bgb my-3 w-100">        
    <div class="borde p-2">

        <div class="col align-self-lg-center">
            <div class="row justify-content-center">

                <!--TITULO Y DESCRIPCION-->
                <div class="col-md-8 col-sm-8">
                    <h1 class="text-left display-4"><?= $model->nombre ?> <span><i class="<?= $estado ?>"></i></span> <span class="navbar"></span></h1>
                    <p class="text-justify"> <?= $model->descripcion ?></p>
                </div>

                <!--arriba derecha-->
                <div class="col-md-4 col-sm-4">
                    <div class="bgb-40 <?= $parar ?>">        
                        <div class="borde">
                            <div class="text-center">
                                <iframe id="player" class="py-5 mt-1 mb-n1" src="https://www.youtube.com/embed/sWtEYPva4A0?modestbranding=1&;showinfo=0&enablejsapi=1&autoplay=<?= $trabajando ?>" frameborder="0" allow="autoplay"></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <!--INSTRUCCIONES-->
                <div class="mt-4 col-md-8  col-sm-8">
                    <div class="bgb">
                        <div class="borde">
                            <h4 class="pl-2 pt-1"> Instrucciones</h4>
                            <div class="instrucciones w-100 asdh-25 mt-3 mb-0 ml-0 bgb row example-1 square scrollbar-dusty-grass thin bordered-z">
                                <div class="col align-self-lg-start">
                                    <div class="row justify-content-start">
                                        <div class="asd">
                                            <div class="">
                                                <!--CONTENIDO EDITABLE-->
                                                <div class="pl-3">
                                                    <?php
                                                    foreach (ArrayHelper::map($model->instrucciones, 'id', 'paso') as $id => $paso) {

//                                                        echo Html::a('<p class="text-justify mr-3">-' . $paso . ' <i class="fa-solid fa-paintbrush"></i></p>', ['instrucciones/editar', 'id' => $id], ['class' => 'a text-white']);

                                                        echo '<a onmouseover="hoverSound(\'' . $_SESSION['dir'] . '\', \'HoverSmall\')" ' .
                                                        'onclick = "clickSound(\'' . $_SESSION['dir'] . '\', \'Mod_Open\', \'instrubtn\', 419)" ' .
                                                        'class = "a text-white"> ' .
                                                        '<p class="text-justify mr-3">-' . $paso . ' <i class="fa-solid fa-paintbrush"></i></p>' .
                                                        '</a>' .
                                                        Html::a('', ['instrucciones/editar', 'id' => $id], ['class' => 'd-none', 'id' => 'instrubtn']);
                                                    }
                                                    ?>
                                                </div>
                                                <div class="pl-3 pb-2">

                                            <!--< ?= Html::a('<i class="fas fa-plus"></i> Añadir instrucción', ['instrucciones/nuevo'], ['class' => 'a px-5 py-2 badge badge-zelda']) ?>-->
                                                    <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                                                       onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'nbtn', 419)" 
                                                       class="a px-5 py-2 badge badge-zelda">
                                                        <i class="fas fa-plus"></i> Añadir instrucción
                                                    </a>
                                                    <?= Html::a('', ['instrucciones/nuevo'], ['class' => 'd-none', 'id' => 'nbtn']) ?>
                                                </div>
                                                <div class="pl-3">

                                                    <!--                                                    < ?php
                                                    //                                                    echo print_r(pos($ls));
                                                    //                                                    echo '<p>debug: </p>';
                                                    //                                                    echo isset($_SESSION['finSesion']);
                                                    //                                                    echo print_r(pos($_SESSION['debug']));
                                                    //                                                    echo '<p>inicio: </p>';
                                                    //                                                    echo pos($ls)['hora_inicio'];
                                                    //                                                    echo $_SESSION['inicioSesion'];
                                                    //                                                    if (isset($_SESSION['inicioSesion'])) {
                                                    //                                                        echo $_SESSION['inicioSesion'];
                                                    //                                                    }
                                                    //                                                    echo '<p>id: <p>';
                                                    
                                                    //                                                    if (isset($_SESSION['idSesion'])) {
                                                    //                                                        echo $_SESSION['idSesion'];
                                                    //                                                    }
                                                    
                                                    //                                                    echo $_SESSION['idSesion'];
                                                    //                                                    echo pos($ls)['id'];
                                                    //                                                    echo print_r($_SESSION['inicioSesion']);
                                                    //                                                    echo '<p>fin: <p>';
                                                    //                                                    echo pos($ls)['hora_fin'];
                                                    //                                                    echo print_r($_SESSION['debug']);
                                                    //                                                    $_SESSION['finSesion']
                                                    //                                                    if (isset($_SESSION['finSesion'])) {
                                                    //                                                        echo $_SESSION['finSesion'];
                                                    //                                                    }
                                                    //                                                    $_SESSION['idSesion'].'<p>';
                                                    //                                                    foreach (ArrayHelper::map($model->sesiones, 'id', 'hora_inicio') as $id => $paso) {
                                                    //                                                        echo Html::a('<p class="text-justify mr-3">-' . $paso . ' <i class="fa-solid fa-paintbrush"></i></p>', ['instrucciones/editar', 'id' => $id], ['class' => 'a text-white']);
                                                    //                                                    }
                                                                                                        ?>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="py-3">
                        <div class="row justify-content-center">
                            <div class="col-md-6 col-sm-6">    
                                <!--< ?= Html::a('', ['tareas/completada', 'id' => $model->id], ['class' => 'bgb btn btn-zelda']) ?>-->
                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'BlueFire_BurnStart', 'marcar', 1000)" 
                                   class="bgb btn btn-zelda">
                                    Marcar tarea como <span class="font-weight-bolder"><?= $completada ?></span>
                                </a>
                                <?= Html::a('', ['tareas/completada', 'id' => $model->id], ['class' => 'd-none', 'id' => 'marcar']) ?>
                            </div>
                        </div>
                    </div>
                </div>


                <!--derecha abajo-->
                <div class="col-md-4 col-sm-4 col align-self-center mb-5 pb-4">
                    <div class="bgb-40 my-3 w-100">        
                        <div class="borde p-5">
                            <div class="<?= $mostrarTiempo ?>text-center">
                                <?php
                                if (isset($_SESSION['tiempo-' . $_SESSION['idTarea']])) {
                                    echo $_SESSION['tiempo-' . $_SESSION['idTarea']];
                                } else {
                                    echo '<p class="shine h4">Sin registros de tiempo</p>';
                                }
                                ?>
                            </div>
                            <div class="<?= $mostrar ?>">

                                <div class="text-center shine pb-5">
                                    <div id="hms"></div>
                                    <div class="d-none start"></div>        
                                    <div class="d-none stop"></div>
                                    <div class="d-none reiniciar"></div>
                                </div>

                            </div>

                            <div class="<?= $mostrar ?>text-right">    
                                <!--BOTONES-->

                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Sent', 'btini', 1005)" 
                                   class="<?= $trabajar ?> bgb btn btn-zelda">
                                    Empezar sesión
                                </a>
                                <?= Html::a('', ['sesiones/nuevo'], ['class' => 'd-none', 'id' => 'btini']) ?>

                                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Receive', 'btparar', 1005)" 
                                   class="<?= $parar ?> bgb btn btn-zelda"
                                   id="boton-parar">
                                    Finalizar sesión
                                </a>
                                <?= Html::a('', ['sesiones/editar', 'id' => $_SESSION['idSesion']], ['class' => 'd-none', 'id' => 'btparar']) ?>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

