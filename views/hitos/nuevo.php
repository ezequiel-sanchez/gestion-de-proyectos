<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Nuevo';
?>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoHito(<?= $_SESSION['idProyecto'] ?>);
        vista('<?= $_SESSION['vista'] ?>');
    });
</script>

<div class="h-100 row text-center">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-3 col-sm-3">
                <!--<p>el id seria: < ?= $id ?></p>-->
                <!--<p>la var submit es: < ?= $_SESSION['submit'] ?></p>-->
                <!--<p>la var id es: <?= $_SESSION['idProyecto'] ?></p>-->
            </div>

            <!--titulo-->
            <div id="form" class="col-md-6 col-sm-6 text-center">
                <?php
                $config = ['template' => "{input}\n{error}\n{hint}"];
                $form = ActiveForm::begin();
                ?>

                <div id="nuevo" class="op80 bgb text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Tiene hitos tu proyecto?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a id="continuar" 
                                   class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="mostrar('nuevo', 'nombre'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="generico('nuevo', 'submit', 'nombre', 'Hito'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">Aún no</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="nombre" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="col align-self-lg-center">

                            <p class="shine text-white display-4">Escribe el título del Hito...</p>
                            <div class="row justify-content-center">
                                <div class="w-50 field my-3"
                                     onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                                     >
                                         <?= $form->field($model, 'nombre_descriptivo')->textInput(['maxlength' => true, 'placeholder' => 'Título del hito...', 'id' => 'nombre-campo'])->label(false) ?>
                                </div>
                            </div>
                            <div class="row justify-content-center">
                                <div class="w-25 my-5 bgb">
                                    <a class="btn btn-zelda"
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                       onclick="siguiente('nombre', 'plazo'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Siguiente</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <!--plazo-->
                <div id="plazo" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="">
                            <p class="shine text-white display-4">¿Tiene fecha el hito?</p>
                        </div>

                        <div class="mt-5 row justify-content-center">

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda"
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="mostrar('plazo', 'fecha'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_Next', 'null', 0)">Si</a>
                            </div>

                            <div class="col-md-1 col-sm-1">
                            </div>

                            <div class="my-5 bgb col-md-4 col-sm-4 text-center">
                                <a class="btn btn-zelda" 
                                   onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                   onclick="generico('plazo', 'submit', 'fecha', 'Plazo'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_No', 'null', 0)">No</a>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="fecha" class="d-none text-center">
                    <div class="borde p-5">
                        <div class="row justify-content-center">

                            <p class="px-5 shine text-white display-4">Selecciona el día que estimas cumplir el hito.</p>

                            <div class="h-100 row justify-content-center field my-5 mb-n1 table table-striped">
                                <div class="caja bgb font-weight-bolder input-group-text">
                                    <?=
                                    $form->field($model, 'fecha_limite')->widget(DatePicker::classname(), [
                                        'type' => DatePicker::TYPE_INLINE,
                                        'options' => [
                                            'class' => 'd-none',
                                            'id' => 'fecha-campo'
                                        ],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                            'todayHighlight' => true,
                                            'todayBtn' => true,
                                            'maxViewMode' => 2,
                                        ]
                                    ])->label(false);
                                    ?>

                                </div>

                                <div class="w-100 row justify-content-center">
                                    <p id="fecha-error" class="w-50 invisible invalid-feedback d-block">error</p>
                                </div>

                            </div>
                            <div class="col-md-4 col-sm-4">
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="bgb">
                                    <a class="btn btn-zelda" 
                                       onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
                                       onclick="siguiente('fecha', 'submit'), clickSound('<?= $_SESSION['dir'] ?>', 'Talk_End', 'null', 0)">Siguiente</a>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                            </div>
                        </div>
                    </div>
                </div>
                <!--submit-->            
                <div  onmouseover="submit()" id="submit" class="d-none text-center">
                    <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->

                    <?= $form->field($model, 'id_proyecto')->textInput(['class' => 'd-none', 'id' => 'proyectoId'])->label(false) ?>

                    <!--finalizar el formulario-->
                    <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-3 col-sm-3">

            </div>

        </div>
    </div>
</div>