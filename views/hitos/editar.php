<?php

use yii\helpers\Html;
?>


<script>
    document.addEventListener("DOMContentLoaded", function () {
        clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Open', 'null', 974);
    });
</script>


<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')" 
                   onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Back', 'bbtn', 419)" 
                   class="a fas fa-angle-left display-3 mt-5 text-white text-center">
                </a>
                <?= Html::a('', ['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']], ['class' => 'd-none', 'id' => 'bbtn']) ?>
            </div> 
        </div> 
    </div>
</div>

<div class="h-100 row">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <div class="col-md-6 col-sm-6 text-center bgb">
                <div class="borde p-5">

                    <p class="shine text-white display-4">Modifica el hito</p>

                    <div id="form" class="vista-mod-altura">
                        <?=
                        $this->render('_form', [
                            'model' => $model,
                        ])
                        ?>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>