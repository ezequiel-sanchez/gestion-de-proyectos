<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;
use kartik\date\DatePicker;
?>

<div class="h-100 row example-1 square scrollbar-dusty-grass square thin bordered-z">
    <div class="col align-self-lg-center">
        <div class="row justify-content-center">

            <?php
            $config = ['template' => "{input}\n{error}\n{hint}"];
            $form = ActiveForm::begin();
            ?>

            <!--< ?= $form->field($model, 'nombre_descriptivo')->textInput(['maxlength' => true]) ?>-->
            <!--NOMBRE-->
            <div class="my-5 pb-2 font-weight-bolder"
                 onclick="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 onkeypress="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverSmall')"
                 >
                     <?=
                             $form
                             ->field($model, 'nombre_descriptivo')
                             ->textInput(['maxlength' => true,
                                 'placeholder' => 'Título del hito...',
                                 'id' => 'nombre-campo',
                                 'class' => 'form-control text-center'])
                             ->label('Título del hito')
                     ?>
            </div>

            <!--< ?= $form->field($model, 'fecha_limite')->textInput() ?>-->
            <!--FECHA LIMITE-->
            <div class="h-75 field asdmt-3 asdmb-5 table table-striped">
                <div class="h-100 caja bgb font-weight-bolder input-group-text asdmt-n5">
                    <?=
                    $form->field($model, 'fecha_limite')->widget(DatePicker::classname(), [
                        'type' => DatePicker::TYPE_INLINE,
                        'options' => [
                            'class' => 'd-none',
                            'id' => 'fecha-campo'
                        ],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'todayBtn' => true,
                            'maxViewMode' => 2,
                        ]
                    ])->label('Fecha límite', ['class' => '']);
                    ?>
                </div>
            </div>

            <?= $form->field($model, 'id_proyecto')->textInput(['class' => 'd-none'])->label(false) ?>

            <a onmouseover="hoverSound('<?= $_SESSION['dir'] ?>', 'HoverBig')" 
               onclick="clickSound('<?= $_SESSION['dir'] ?>', 'Mod_Close', 'sbtn', 663)" 
               class="bgb btn btn-zelda my-5"
               id="submit-button">
                Guardar
            </a>
            <?= Html::submitButton('', ['class' => 'd-none', 'name' => 'siguiente-button', 'id' => 'sbtn']) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>