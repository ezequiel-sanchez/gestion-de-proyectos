<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sesiones */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoTerminarSesion();
    });
</script>

<div class="d-none sesiones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'duracion')->textInput(['class' => 'dd-none', 'id' => 'duracion'])->label() ?>

    <?= $form->field($model, 'hora_inicio')->textInput(['class' => 'dd-none', 'id' => 'hInicio'])->label() ?>

    <?= $form->field($model, 'hora_fin')->textInput(['class' => 'dd-none', 'id' => 'hFin'])->label() ?>

    <?= $form->field($model, 'resumen_trabajo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_tarea')->textInput(['class' => ['dd-none']])->label() ?>

    <div class="form-group">
        <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
