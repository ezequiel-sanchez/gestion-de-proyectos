<?php

use yii\bootstrap4\Html;
use kartik\form\ActiveForm;

$this->title = 'Nuevo';
?>

<script>
    document.addEventListener("DOMContentLoaded", function () {
        rellenoIniciarSesion(<?= $_SESSION['idTarea'] ?>);
    });
</script>

<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <?= Html::a('', [''], ['class' => 'a fas fa-angle-left display-3 mt-5 text-white text-center']) ?>
            </div> 
        </div> 

        <div class="col-md-1 col-sm-1 mt-7 ml-6 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <?= Html::a('', [''], ['class' => 'a fa-solid fa-paintbrush display-3 mt-5 text-white text-center']) ?>
            </div> 
        </div> 
    </div>
</div>

<div id="form" class="d-none col-md-6 col-sm-6 text-center">
    <?php
    $config = ['template' => "{input}\n{error}\n{hint}"];
    $form = ActiveForm::begin();
    ?>

    <!--submit-->            
    <div  onmouseover="submit()" id="submit" class="dd-none text-center">

        <!--estos campos se rellenaran con javascript o quedaran vacios segun haga falta-->
        <?= $form->field($model, 'duracion')->textInput() ?>
        <?= $form->field($model, 'resumen_trabajo')->textarea(['rows' => '6', 'maxlength' => true, 'placeholder' => 'Resumen de la sesión...', 'class' => 'dd-none', 'id' => 'descripcion-campo']) ?>
        <?= $form->field($model, 'hora_inicio')->textInput(['class' => 'dd-none', 'id' => 'hInicio']) ?>
        <?= $form->field($model, 'hora_fin')->textInput(['class' => 'dd-none', 'id' => 'hFin']) ?>
        <?= $form->field($model, 'id_tarea')->textInput(['class' => 'dd-none', 'id' => 'tareaId']) ?>

        <!--finalizar el formulario-->
        <?= Html::submitButton('Siguiente', ['class' => 'd-none btn btn-zelda', 'name' => 'siguiente-button', 'id' => 'submit-button']) ?>

    </div>
    <?php ActiveForm::end(); ?>
</div>




<!--< ?= $form->field($model, 'duracion')->textInput() ?>-->

<!--< ?= $form->field($model, 'hora_inicio')->textInput() ?>-->

<!--< ?= $form->field($model, 'hora_fin')->textInput() ?>-->

<!--< ?= $form->field($model, 'resumen_trabajo')->textInput(['maxlength' => true]) ?>-->

<!--< ?= $form->field($model, 'id_tarea')->textInput() ?>-->