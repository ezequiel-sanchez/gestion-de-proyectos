<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Sesiones */

$this->title = 'Update Sesiones: ' . $model->id;
//$this->params['breadcrumbs'][] = ['label' => 'Sesiones', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
//$this->params['breadcrumbs'][] = 'Update';
?>

<!--BOTONES HOME, BACK, EDITAR-->
<div class="col align-self-lg-center">
    <div class="row justify-content-center">
        <div class="col-md-1 col-sm-1 mt-7 ml-5 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <?= Html::a('', [''], ['class' => 'a fas fa-angle-left display-3 mt-5 text-white text-center']) ?>
            </div> 
        </div> 

        <div class="col-md-1 col-sm-1 mt-7 ml-6 fixed-top"> 
            <div class="col-md-1 col-sm-3">
                <?= Html::a('', [''], ['class' => 'a fa-solid fa-paintbrush display-3 mt-5 text-white text-center']) ?>
            </div> 
        </div> 
    </div>
</div>

<div class="d-none sesiones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
