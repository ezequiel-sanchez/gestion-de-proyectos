<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosAObtener */

$this->title = 'Create Productos A Obtener';
$this->params['breadcrumbs'][] = ['label' => 'Productos A Obteners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-aobtener-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
