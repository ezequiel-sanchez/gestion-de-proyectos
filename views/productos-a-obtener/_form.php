<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosAObtener */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="productos-aobtener-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_objetivo')->textInput() ?>

    <?= $form->field($model, 'resultado')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
