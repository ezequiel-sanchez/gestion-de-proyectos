<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Logros;
use app\models\ContactForm;
use app\models\ValidarFormulario;
use app\models\ValidarFormularioAjax;
use yii\widgets\ActiveForm;
use app\models\FormRegister;
use app\models\Users;
use yii\web\Session;
use app\models\FormRecoverPass;
use app\models\FormResetPass;
use yii\helpers\Url;
use yii\helpers\Html;
use app\controllers\ProyectosController;

// ejemplo de pruebas del proyecto: "C:\xampp\htdocs\dam\proyectos\gestionproyectos"
// direccion de la carpeta del proyecto posterior al htdocs: --> 'dam\proyectos\gestionproyectos' <--
//substr(Yii::$app->homeUrl, 1,-14) = substr(Yii::$app->homeUrl, 1,-14); //obtener automaticamente la direccion
$homeUrl = Yii::$app->homeUrl;
$_SESSION['dir'] = substr($homeUrl, 1, -14);

if (!isset($_SESSION['proyecto'])) {
    $_SESSION['proyecto'] = "null";
}
if (!isset($_SESSION['plazo'])) {
    $_SESSION['plazo'] = "null";
}
if (!isset($_SESSION['tiempo'])) {
    $_SESSION['tiempo'] = "null";
}
if (!isset($_SESSION['totalHitos'])) {
    $_SESSION['totalHitos'] = "null";
}
if (!isset($_SESSION['totalObjetivos'])) {
    $_SESSION['totalObjetivos'] = "null";
}
if (!isset($_SESSION['totalTareas'])) {
    $_SESSION['totalTareas'] = "null";
}
if (!isset($_SESSION['tipo'])) {
    $_SESSION['tipo'] = "null";
}
if (!isset($_SESSION['idObjetivo'])) {
    $_SESSION['idObjetivo'] = "null";
}
if (!isset($_SESSION['idHito'])) {
    $_SESSION['idHito'] = "null";
}
if (!isset($_SESSION['pagProyecto'])) {
    $_SESSION['pagProyecto'] = 0;
}

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {

//        $_SESSION['dir'] = substr(Yii::$app->homeUrl, 1, -14); //obtener automaticamente la direccion

        $_SESSION['continuar'] = false;
        $_SESSION['layout'] = false;
        $_SESSION['idProyecto'] = null;
        return $this->render('index');
    }

    public function actionComenzar() {
//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Comenzar') { //con el 'or' no devolvemos al usuario al main al recargar la pagina
        return $this->redirect(['proyectos/nuevo']);
//        }
//        return $this->render('index');
    }

    public function actionContinuar() {
//            if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo') {
        if (isset($_SESSION['idProyecto'])) {
            return $this->redirect(['proyectos/continuar', 'id' => $_SESSION['idProyecto'], /* 'hitos'=> "null", */ 'hito' => $_SESSION['idHito']]);
        } else {
        return $this->redirect(['proyectos/seleccionar']);
        }
//    }
//        return $this->render('index');
    }

    public function actionLogros() {
        $model = UsersController::findThis(Yii::$app->user->getId());

        $proyectos = $model->proyectos;

        $horas = 0;
        $minutos = 0;
        $segundos = 0;
        $totalHitos = 0;
        $hitosCompletados = 0;
        $objetivosCompletados = 0;
        $totalObjetivos = 0;
        $tareasCompletadas = 0;
        $totalTareas = 0;

        foreach ($proyectos as $proyecto) {
            $horas += ProyectosController::updateLayout($proyecto->id, 'horas');
            $minutos += ProyectosController::updateLayout($proyecto->id, 'minutos');
            $segundos += ProyectosController::updateLayout($proyecto->id, 'segundos');
            $hitosCompletados += ProyectosController::updateLayout($proyecto->id, 'hitosCompletados');
            $totalHitos += ProyectosController::updateLayout($proyecto->id, 'totalHitos');
            $objetivosCompletados += ProyectosController::updateLayout($proyecto->id, 'objetivosCompletados');
            $totalObjetivos += ProyectosController::updateLayout($proyecto->id, 'totalObjetivos');
            $tareasCompletadas += ProyectosController::updateLayout($proyecto->id, 'tareasCompletadas');
            $totalTareas += ProyectosController::updateLayout($proyecto->id, 'totalTareas');
        }

        $minutos += (int) ($segundos / 60);

        $horas += (int) ($minutos / 60);
        if ($horas < 10) {
            $horas = '0' . $horas;
        }

        $minutos -= 60 * (int) ($minutos / 60);
        if ($minutos < 10) {
            $minutos = '0' . $minutos;
        }

        $segundos -= 60 * (int) ($segundos / 60);
        if ($segundos < 10) {
            $segundos = '0' . $segundos;
        }

        $tiempo = 'Total: ' . $horas . ':' . $minutos/* . ' segundos: '.$segundos */;

        return $this->render('logros', [
                    'horas' => $horas,
                    'minutos' => $minutos,
                    'segundos' => $segundos,
                    'tiempo' => $tiempo,
                    'tiempo' => $tiempo,
                    'totalHitos' => $totalHitos,
                    'hitosCompletados' => $hitosCompletados,
                    'objetivosCompletados' => $objetivosCompletados,
                    'totalObjetivos' => $totalObjetivos,
                    'tareasCompletadas' => $tareasCompletadas,
                    'totalTareas' => $totalTareas,
                    'nProyectos' => count($proyectos),
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {
//        if (!isset($_SESSION['presentacion'])) {
//            $_SESSION['presentacion'] = '';
//            $_SESSION['iniciar'] = 'd-none';
//        } else {
        $_SESSION['presentacion'] = 'd-none';
        $_SESSION['iniciar'] = '';
//        }
        $_SESSION['layout'] = 0;
        $_SESSION['dir'] = substr(Yii::$app->homeUrl, 1, -14); //obtener automaticamente la direccion

        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->contraseña = '';
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSalir() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionCrud() {
        $_SESSION['presentacion'] = 'd-none';
        $_SESSION['iniciar'] = '';
        return $this->redirect(['site/login']);
    }

    // tut 1




    public function actionFormulario($mensaje = null) {
        return $this->render("formulario", ["mensaje" => $mensaje]);
    }

    public function actionRequest() {
        $mensaje = null;
        if (isset($_REQUEST["nombre"])) {
            $mensaje = "Bien, has enviando tu nombre correctamente: " . $_REQUEST["nombre"];
        }
        $this->redirect(["site/formulario", "mensaje" => $mensaje]);
    }

    public function actionValidarformulario() {

        $model = new ValidarFormulario;
        $msg = null;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Por ejemplo, consultar en una base de datos
                $msg = "funciona";
                $model->nombre = null;
                $model->email = null;
            } else {
                $model->getErrors();
            }
        }

        return $this->render("validarformulario", ["model" => $model, 'msg' => $msg]);
    }

    public function actionValidarformularioajax() {
        $model = new ValidarFormularioAjax;
        $msg = null;

        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Por ejemplo hacer una consulta a una base de datos
                $msg = "Enhorabuena formulario enviado correctamente";
                $model->nombre = null;
                $model->email = null;
            } else {
                $model->getErrors();
            }
        }

        return $this->render("validarformularioajax", ['model' => $model, 'msg' => $msg]);
    }

//    public function behaviors()
//    {
//        return [
//            'access' => [
//                'class' => AccessControl::className(),
//                'only' => ['logout'],
//                'rules' => [
//                    [
//                        'actions' => ['logout'],
//                        'allow' => true,
//                        'roles' => ['@'],
//                    ],
//                ],
//            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
//        ];
//    }
//    public function actions()
//    {
//        return [
//            'error' => [
//                'class' => 'yii\web\ErrorAction',
//            ],
//            'captcha' => [
//                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
//            ],
//        ];
//    }
//    public function actionIndex()
//    {
//        return $this->render('index');
//    }
//    public function actionLogin()
//    {
//        if (!\Yii::$app->user->isGuest) {
//            return $this->goHome();
//        }
//
//        $model = new LoginForm();
//        if ($model->load(Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
//        } else {
//            return $this->render('login', [
//                'model' => $model,
//            ]);
//        }
//    }
//    public function actionLogout()
//    {
//        Yii::$app->user->logout();
//
//        return $this->goHome();
//    }
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        } else {
//            return $this->render('contact', [
//                'model' => $model,
//            ]);
//        }
//    }
//    public function actionAbout()
//    {
//        return $this->render('about');
//    }
// CODIGO PARA LOS REGISTROS

    private function randKey($str = '', $long = 0) {
        $key = null;
        $str = str_split($str);
        $start = 0;
        $limit = count($str) - 1;
        for ($x = 0; $x < $long; $x++) {
            $key .= $str[rand($start, $limit)];
        }
        return $key;
    }

    public function actionConfirm() {
//        echo "hola actionConfirm";
        $table = new Users;
        $authKey = null;
//        Users::findOne($id)->estado=1;
//        $table->update();
        if (Yii::$app->request->get()) {

            //Obtenemos el valor de los parámetros get
            $id = Html::encode($_GET["id"]);
            //$id = $_GET["id"];
            $authKey = $_GET["authKey"];



            if ((int) $id) {
                //Realizamos la consulta para obtener el registro
                $model = $table
                        ->find()
                        ->where("id=:id", [":id" => $id])
                        ->andWhere("authKey=:authKey", [":authKey" => $authKey]);

                //Si el registro existe
                if ($model->count() == 1) {
                    $activar = Users::findOne($id);
                    $activar->estado = 1;
                    if ($activar->update()) {
//                        echo "<br/>ok<br/>";
                        echo "<h1>REGISTRO EXITOSO, redireccionando...</h1><br/>";
                        echo "<meta http-equiv='refresh' content='2; " . Url::toRoute("site/login") . "'>";
                    } else {
                        echo "<br/>El usuario ya esta activado, redireccionando...<br/>";
//                        echo "Ha ocurrido un error al realizar el registro, redireccionando ...<br/>";
                        echo "<meta http-equiv='refresh' content='5; " . Url::toRoute("site/login") . "'>";
                    }
                } else { //Si no existe redireccionamos a login
                    echo "<br/>No existe el registro, redireccionando...<br/>";
                    //return $this->redirect(["site/login"]);
//                    echo "Ha ocurrido un error al realizar el registro, redireccionando ...<br/>";
                    echo "<meta http-equiv='refresh' content='5; " . Url::toRoute("site/login") . "'>";
                }
            } else { //Si id no es un número entero redireccionamos a login
//                echo "<br/>error3  el id no es numero entero<br/>";
//                return $this->redirect(["site/login"]);
                echo "Ha ocurrido un error al realizar el registro, redireccionando...<br/>";
                echo "<meta http-equiv='refresh' content='5; " . Url::toRoute("site/login") . "'>";
            }
        }
    }

    public function actionRegister() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        //Creamos la instancia con el model de validación
        $model = new FormRegister;

        //Mostrará un mensaje en la vista cuando el usuario se haya registrado
        $msg = null;

        //Validación mediante ajax
        if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        //Validación cuando el formulario es enviado vía post
        //Esto sucede cuando la validación ajax se ha llevado a cabo correctamente
        //También previene por si el usuario tiene desactivado javascript y la
        //validación mediante ajax no puede ser llevada a cabo
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Preparamos la consulta para guardar el usuario
                $table = new Users;
                $table->username = $model->username;
                $table->email = $model->email;
                //Encriptamos el password
                $table->password = crypt($model->password, Yii::$app->params["salt"]);
                //Creamos una cookie para autenticar al usuario cuando decida recordar la sesión, esta misma
                //clave será utilizada para activar el usuario
                $table->authKey = $this->randKey("abcdef0123456789", 200);
                //Creamos un token de acceso único para el usuario
                $table->accessToken = $this->randKey("abcdef0123456789", 200);

                //Si el registro es guardado correctamente
                if ($table->insert()) {
                    //Nueva consulta para obtener el id del usuario
                    //Para confirmar al usuario se requiere su id y su authKey
                    $user = $table->find()->where(["email" => $model->email])->one();
                    $id = urlencode($user->id);
                    $authKey = urlencode($user->authKey);

                    $subject = "Confirmar registro";
                    $body = "<div style=\"
                                width: 25%;
                                margin: 0% 0% 0% 5%;
                                border: 1px solid transparent;
                                background: rgba(0, 0, 0, 0.9);
                                border-radius: 7px;
                                
                                    color: white !important;
                                    display: inline-block;
                                    font-weight: 400;
                                    text-align: center;
                                    vertical-align: middle;
                                    -webkit-user-select: none;
                                    -moz-user-select: none;
                                    -ms-user-select: none;
                                    user-select: none;
                                    padding: 0.15rem 0.15rem;
                                    font-size: 1rem;
                                    line-height: 1.5;
                            \">
                                    <div style=\"
                                        border: 3px solid white;
                                        border-color: rgba(255, 255, 255, 0.2);
                                        border-radius: 3px;
                                        width: 100%;
                                        font-family: sans-serif;
                                        
                                        display: inline-block;
                                        vertical-align: middle;
                                        -webkit-user-select: none;
                                        -moz-user-select: none;
                                        -ms-user-select: none;
                                        user-select: none;
                                        padding: 2.5rem 0.0rem;
                                        font-size: 1rem;
                                        line-height: 1.5;
                                        
                                    \">
                                        <h3 style=\"font-family: sans-serif\">Enhorabuena, para finalizar el registro haga click en aceptar...</h3>
                                        <div style=\"
                                            margin-left: 25%;
                                            width: 100%;
                                            padding: 30px 30px 0px 0px;
                                        \">
                                            
                                        
                                            <div style=\"
                                                border: 1px solid transparent;
                                                background: rgba(0, 0, 0, 0.6) !important;
                                                border-radius: 7px;
                                                padding: 3px 3px 3px 3px;
                                                width: 50%;
                                            \">
                                                <a style=\"
                                                    background: rgba(0, 0, 0, 0.1);
                                                    border-color: rgba(255, 255, 255, 0.5) !important;
                                                    border-radius: 3px;
                                                    padding: 15px 0px 15px 0px;
                                                    width: 100%;
                                                    text-decoration: none;
                                                    
                                                    color: white !important;
                                                    display: inline-block;
                                                    font-weight: 400;
                                                    color: #212529;
                                                    text-align: center;
                                                    vertical-align: middle;
                                                    -webkit-user-select: none;
                                                    -moz-user-select: none;
                                                    -ms-user-select: none;
                                                    user-select: none;
                                                    background-color: transparent;
                                                    border: 1px solid transparent;
                                                    padding: 0.375rem 0.75rem;
                                                    font-size: 1rem;
                                                    line-height: 1.5;
                                                    border-radius: 0.25rem;
                                                \"
                                                    href=\"http://localhost/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/confirm?&id=" . $id . "&authKey=" . $authKey . "\">Aceptar
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                            </div>";
                    //Enviamos el correo
                    Yii::$app->mailer->compose()
                            ->setTo($user->email)
                            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                            ->setSubject($subject)
                            ->setHtmlBody($body)
                            ->send();

                    $model->username = null;
                    $model->email = null;
                    $model->password = null;
                    $model->password_repeat = null;

                    $msg = "<div class=\"row justify-content-center op80 bgb\">
                                <div class=\"borde p-5 align-self-center\">
                                    <h3 class=\"text-justify\">Enhorabuena, ahora sólo hace falta que confirmes tu registro desde tu cuenta de correo electrónico.</h3>
                                    <div class=\"mt-5 col row justify-content-center\">
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                        <div class=\"col-md-6 col-sm-6 bgb\">
                                            <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/login\">Ok</a>
                                        </div>
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>";
                } else {
                    $msg = "<div class=\"row justify-content-center op80 bgb\">
                                <div class=\"borde p-5 align-self-center\">
                                    <h3 class=\"text-justify\">Ha ocurrido un error al llevar a cabo tu registro.</h3>
                                    <div class=\"mt-5 col row justify-content-center\">
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                        <div class=\"col-md-6 col-sm-6 bgb\">
                                            <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/register\">Ok</a>
                                        </div>
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>";
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("register", ["model" => $model, "msg" => $msg]);
    }

    // FIN DEL CODIGO PARA LOS REGISTROS
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    // INICIO CODIGO RECUPERAR CONTRASEÑAS


    public function actionRecoverpass() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        //Instancia para validar el formulario
        $model = new FormRecoverPass;

        //Mensaje que será mostrado al usuario en la vista
        $msg = null;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Buscar al usuario a través del email
                $table = Users::find()->where("email=:email", [":email" => $model->email]);

                //Si el usuario existe
                if ($table->count() == 1) {
                    //Crear variables de sesión para limitar el tiempo de restablecido del password
                    //hasta que el navegador se cierre
                    $session = new Session;
                    $session->open();

                    //Esta clave aleatoria se cargará en un campo oculto del formulario de reseteado
                    $session["recover"] = $this->randKey("abcdef0123456789", 200);
                    $recover = $session["recover"];

                    //También almacenaremos el id del usuario en una variable de sesión
                    //El id del usuario es requerido para generar la consulta a la tabla users y 
                    //restablecer el password del usuario
                    $table = Users::find()->where("email=:email", [":email" => $model->email])->one();
                    $session["id_recover"] = $table->id;

                    //Esta variable contiene un número hexadecimal que será enviado en el correo al usuario 
                    //para que lo introduzca en un campo del formulario de reseteado
                    //Es guardada en el registro correspondiente de la tabla users
                    $verificationCode = $this->randKey("abcdef0123456789", 8);
                    //Columna verificationCode
                    $table->verificationCode = $verificationCode;
                    //Guardamos los cambios en la tabla users
                    $table->save();

                    //Creamos el mensaje que será enviado a la cuenta de correo del usuario
                    $subject = "Recuperar password";
                    $body = "<p>Copie el siguiente código de verificación para restablecer su password ... ";
                    $body .= "<strong>" . $verificationCode . "</strong></p>";
                    //$body .= "<p><a href='http://yii.local/index.php?r=site/resetpass'>Recuperar password</a></p>";
                    $body .= "<p><a href='http://localhost/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/resetpass'>Recuperar password</a></p>";

                    //Enviamos el correo
                    Yii::$app->mailer->compose()
                            ->setTo($model->email)
                            ->setFrom([Yii::$app->params["adminEmail"] => Yii::$app->params["title"]])
                            ->setSubject($subject)
                            ->setHtmlBody($body)
                            ->send();

                    //Vaciar el campo del formulario
                    $model->email = null;

                    //Mostrar el mensaje al usuario
                    $msg = "<div class=\"row justify-content-center op80 bgb\">
                                <div class=\"borde p-5 align-self-center\">
                                    <h3 class=\"text-justify\">Le hemos enviado un mensaje a su cuenta de correo electrónico para que pueda restablecer su contraseña.</h3>
                                    <div class=\"mt-5 col row justify-content-center\">
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                        <div class=\"col-md-6 col-sm-6 bgb\">
                                            <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/login\">Ok</a>
                                        </div>
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>";
                } else { //El usuario no existe
                    $msg = "<div class=\"row justify-content-center op80 bgb\">
                                <div class=\"borde p-5 align-self-center\">
                                    <h3 class=\"text-justify\">Ha ocurrido un error.</h3>
                                    <div class=\"mt-5 col row justify-content-center\">
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                        <div class=\"col-md-6 col-sm-6 bgb\">
                                            <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/recoverpass\">Ok</a>
                                        </div>
                                        
                                        <div class=\"col-md-3 col-sm-3\">
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>";
                }
            } else {
                $model->getErrors();
            }
        }
        return $this->render("recoverpass", ["model" => $model, "msg" => $msg]);
    }

    public function actionResetpass() {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        //Instancia para validar el formulario
        $model = new FormResetPass;

        //Mensaje que será mostrado al usuario
        $msg = null;

        //Abrimos la sesión
        $session = new Session;
        $session->open();

        //Si no existen las variables de sesión requeridas lo expulsamos a la página de inicio
        if (empty($session["recover"]) || empty($session["id_recover"])) {
            return $this->redirect(["site/index"]);
        } else {

            $recover = $session["recover"];
            //El valor de esta variable de sesión la cargamos en el campo recover del formulario
            $model->recover = $recover;

            //Esta variable contiene el id del usuario que solicitó restablecer el password
            //La utilizaremos para realizar la consulta a la tabla users
            $id_recover = $session["id_recover"];
        }

        //Si el formulario es enviado para resetear el password
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                //Si el valor de la variable de sesión recover es correcta
                if ($recover == $model->recover) {

                    //Preparamos la consulta para resetear el password, requerimos el email, el id 
                    //del usuario que fue guardado en una variable de session y el código de verificación
                    //que fue enviado en el correo al usuario y que fue guardado en el registro
                    $table = Users::findOne(["email" => $model->email, "id" => $id_recover, "verificationCode" => $model->verificationCode]);

                    //comprobar que exista
                    if ($table !== null) {

                        //Encriptar el password
                        $table->password = crypt($model->password, Yii::$app->params["salt"]);


                        //Si la actualización se lleva a cabo correctamente
                        if ($table->save()) {

                            //Destruir las variables de sesión
                            $session->destroy();

                            //Vaciar los campos del formulario
                            $model->email = null;
                            $model->password = null;
                            $model->password_repeat = null;
                            $model->recover = null;
                            $model->verificationCode = null;

                            $msg = "<div class=\"row justify-content-center op80 bgb\">
                                    <div class=\"borde p-5 align-self-center\">
                                        <h3 class=\"text-justify\">Enhorabuena, contraseña restablecida correctamente, redireccionando a la página de login...</h3>
                                        <div class=\"mt-5 col row justify-content-center\">
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                            <div class=\"col-md-6 col-sm-6 bgb\">
                                                <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/login\">Ok</a>
                                            </div>
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>";
                            $msg .= "<meta http-equiv='refresh' content='5; " . Url::toRoute("site/login") . "'>";
                        } else {
                            $msg = "<div class=\"row justify-content-center op80 bgb\">
                                    <div class=\"borde p-5 align-self-center\">
                                        <h3 class=\"text-justify\">Ha ocurrido un error al guardar el registro.</h3>
                                        <div class=\"mt-5 col row justify-content-center\">
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                            <div class=\"col-md-6 col-sm-6 bgb\">
                                                <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/resetpass\">Ok</a>
                                            </div>
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>";
                        }
                    } else {
                        $msg = "<div class=\"row justify-content-center op80 bgb\">
                                    <div class=\"borde p-5 align-self-center\">
                                        <h3 class=\"text-justify\">El código de verificacion no coincide con la direccion de correo electrónico.</h3>
                                        <div class=\"mt-5 col row justify-content-center\">
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                            <div class=\"col-md-6 col-sm-6 bgb\">
                                                <a class=\"btn btn-zelda\" href=\"/" . substr(Yii::$app->homeUrl, 1, -14) . "/web/index.php/site/resetpass\">Ok</a>
                                            </div>
                                        
                                            <div class=\"col-md-3 col-sm-3\">
                                            </div>
                                        
                                        </div>
                                    </div>
                                </div>";
                    }
                } else {
                    $model->getErrors();
                }
            }
        }

        return $this->render("resetpass", ["model" => $model, "msg" => $msg]);
    }

    // FIN DEL CODIGO PARA RECUPERAR CONTRASEÑAS
}

// a ver creo que entiendo por que lo hace aasi, no se, tengo que verlo un poco mas a fondo pero, 
//        si hiciera la validacion al momento de crearlo dentro del modelo del usuario por ejemplo, te permitiria crear el usuario repetido por ejemplo
// pero aun asi, si pudiera hacerlo todo desde el propio modelo users quedaria mas limpio no?
// validar que los datos estan correctos, que no se repiten y solo asi permitir hacer el submit y enviar el correo