<?php

namespace app\controllers;

//use app\controllers\ProyectosController;
use app\models\Tareas;
use app\models\Proyectos;
use app\models\Sesiones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * TareasController implements the CRUD actions for Tareas model.
 */
class TareasController extends Controller {

    public function actionNuevo() {
        $_SESSION['layout'] = false;

//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo' || $_SESSION['vista'] == 'Continuar') {

            $model = new Tareas();

            if ($this->request->isPost) {

                if ($model->load($this->request->post()) && $model->save()) {
                    $_SESSION['idTarea'] = $model->id;
                    return $this->redirect(['instrucciones/nuevo']);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('nuevo', [
                        'model' => $model,
            ]);
//        }
//        return $this->redirect(["site/index"]);
    }

    public function actionContinuar($id) {
        ProyectosController::updateLayout($_SESSION['idProyecto'],'update');
        unset($_SESSION['idSesion']);
        unset($_SESSION['inicioSesion']);
        unset($_SESSION['finSesion']);
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;
        $_SESSION['continuar'] = true;
        $_SESSION['idTarea'] = $model->id;
        $map = ArrayHelper::map($model->sesiones, 'hora_inicio', 'hora_fin', 'id');
        foreach ($map as $i => $hInicio) {
            $_SESSION['idSesion'] = $i;
            foreach ($map[$i] as $x => $y) {
                $_SESSION['inicioSesion'] = $x;
                $_SESSION['finSesion'] = $y;
            }

//            $_SESSION['debug'] = $map;
//            $_SESSION['idSesion'] = ArrayHelper::getColumn($sesion, 'id');
//            $_SESSION['inicioSesion'] = ArrayHelper::getColumn($sesion, 'hora_inicio');
//                                                        $_SESSION['inicioSesion'] = strval(array_column($model->sesiones, 'hora_inicio'));
//                                                        $_SESSION['finSesion'] = print_r($model->sesiones);
//                                                $_SESSION['idSesion'] = $model->id;$duracion = preg_split("/:/", $time);
//                                                $horas += intval($duracion[0]);
//                                                $minutos += intval($duracion[1]);
//                                                $segundos += intval($duracion[2]);
        }
        $ls = new ActiveDataProvider([
//            'query' => Objetivos::find()->where(['in', 'id', array_values(ArrayHelper::map($objetivos, 'id_objetivo', 'id_objetivo'))]),
            'query' => Sesiones::find()->where("id = (select max(id) from sesiones) && id_tarea=" . $_SESSION['idTarea'])->one()
        ]);
        $_SESSION['debug'] = $ls;
//        $_SESSION['idSesion'] = $model->id;
//        
//        $_SESSION['proyecto'] = $model->titulo_proyecto;
//        $_SESSION['layout'] = true;
//        $_SESSION['continuar'] = true;
//        $_SESSION['idHito'] = $hito;
//        $_SESSION['plazo'] = $model->plazo_entrega;
//        $_SESSION['tipo'] = $model->tipo;
//        switch ($_SESSION['tipo']) {
//            case 'Personal':
//                $_SESSION['tipo'] = '<i class="fa-solid fa-mug-hot"></i> ';
//                break;
//            case 'Estudios':
//                $_SESSION['tipo'] = '<i class="fa-solid fa-graduation-cap"></i> ';
//                break;
//            case 'Investigación':
//                $_SESSION['tipo'] = '<i class="fa-solid fa-microscope"></i> ';
//                break;
//            default:
//                $_SESSION['tipo'] = '<i class="fa-solid fa-seedling"></i> ';
//        }
//        if (strlen($_SESSION['plazo']) === 0) {
//            $_SESSION['plazo'] = 'Sin plazo de entrega';
//        }
//        if (isset($_SESSION['idProyecto']) || isset($id)) {
//            if (isset($id)) {
//                $_SESSION['idProyecto'] = $id;
//            }
//            $hitos = ArrayHelper::map($model->hitos, 'id', 'nombre_descriptivo');
//            $objetivos = Yii::$app->db->createCommand(
//                            'SELECT DISTINCT id_objetivo FROM objetivos_en_hitos
//                             WHERE id_hito ='
//                            . $hito)->queryAll();
        $dataProvider = new ActiveDataProvider([
//            'query' => Objetivos::find()->where(['in', 'id', array_values(ArrayHelper::map($objetivos, 'id_objetivo', 'id_objetivo'))]),
            'query' => Tareas::find()->where("id =" . $id)
        ]);
        return $this->render('continuar', [
                    'dp' => $dataProvider,
                    'titulo' => $model->nombre,
                    'ls' => $ls,
//                        'id' => $hito,
//                        'hitos' => $hitos,
//                        'proyecto' => $model->titulo_proyecto,
        ]);
//        } else {
//            $this->redirect(['site/index']);
//        }
    }

    public function actionEditar($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['tareas/continuar', 'id' => $_SESSION['idTarea']]);
        }

        return $this->render('editar', [
                    'model' => $model,
        ]);
    }

    public function actionCompletada($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['tareas/continuar', 'id' => $_SESSION['idTarea']]);
        }

        return $this->render('completada', [
                    'model' => $model,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Tareas models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Tareas::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tareas model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tareas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Tareas();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tareas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tareas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tareas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Tareas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Tareas::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
