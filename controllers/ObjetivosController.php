<?php

namespace app\controllers;

use app\models\Objetivos;
use app\models\Tareas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ObjetivosController implements the CRUD actions for Objetivos model.
 */
class ObjetivosController extends Controller {

    public function actionNuevo() {
        $_SESSION['layout'] = false;
//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo' || $_SESSION['vista'] == 'Continuar') {

            $model = new Objetivos();

            if ($this->request->isPost) {

                if ($model->load($this->request->post()) && $model->save()) {
                    $_SESSION['idObjetivo'] = $model->id;
                    return $this->redirect(['productos-a-obtener/nuevo']);
                }
//                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('nuevo', [
                        'model' => $model,
            ]);
//        }
//        return $this->redirect(["site/index"]);
    }

    public function actionContinuar($id) {
        ProyectosController::updateLayout($_SESSION['idProyecto'],'update');
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;
        $_SESSION['continuar'] = true;
        $productos = ArrayHelper::getColumn($model->productosAObteners, 'resultado');
        $_SESSION['idObjetivo'] = $id;

        $dataProvider = new ActiveDataProvider([
            'query' => Tareas::find()->where("id_objetivo =" . $id)
        ]);

        return $this->render('continuar', [
                    'dp' => $dataProvider,
                    'titulo' => $model->titulo,
                    'productos' => $productos,
        ]);
    }

    public function actionEditar($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['objetivos/continuar', 'id' => $_SESSION['idObjetivo'], 'hito' => $_SESSION['idHito']]);
        }

        return $this->render('editar', [
                    'model' => $model,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Objetivos models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Objetivos::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Objetivos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Objetivos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Objetivos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Objetivos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Objetivos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Objetivos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Objetivos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Objetivos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
