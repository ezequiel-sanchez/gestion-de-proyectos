<?php

namespace app\controllers;

use app\models\Proyectos;
use app\models\Objetivos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

class ProyectosController extends Controller {

    public function actionNuevo() {
//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo') { //con el 'or' no devolvemos al usuario al main al recargar la pagina
//            $id = Yii::$app->db->createCommand(
//                            'SELECT id FROM users
//                             WHERE username = "' . Yii::$app->user->identity->username . '"')->queryScalar();
        $cuenta = Yii::$app->db->createCommand(
                        'SELECT count(*) FROM proyectos
                             WHERE id_user = (select id from users where username = "' . Yii::$app->user->identity->username . '")')->queryScalar();

//            'query' => Proyectos::find()->count()->where('username =' . Yii::$app->user->identity->username),
//                ]);
        $model = new Proyectos();

//            if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ActiveForm::validate($model);
//            }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                $_SESSION['idProyecto'] = $model->id;
//                    $_SESSION['submit']= 0;
                return $this->redirect(['hitos/nuevo']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('nuevo', [
                    'model' => $model,
                    'cuenta' => $cuenta + 1,
                    'id' => Yii::$app->user->getId(),
        ]);
//        }
//        return $this->redirect(["site/index"]);
    }

    public function actionSeleccionar() {
        $_SESSION['layout'] = false;

        if (!isset($_SESSION['pagProyecto'])) {
            $_SESSION['pagProyecto'] = 0;
        }

//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Seleccionar' || $_SESSION['vista'] == 'Continuar') {

        $dataProvider = new ActiveDataProvider([
            'query' => Proyectos::find()->where("id_user =" . Yii::$app->user->getId())
        ]);

        return $this->render('seleccionar', [
                    'dp' => $dataProvider,
        ]);
//        } else {
//            $this->redirect(['site/index']);
//        }
    }

    public function actionContinuar($id, $hito) {
        $model = $this->findModel($id);

        if (!isset($_SESSION['pagProyecto'])) {
            $_SESSION['pagProyecto'] = 0;
        }

        $_SESSION['proyecto'] = $model->titulo_proyecto;
        $_SESSION['layout'] = true;
        $_SESSION['continuar'] = true;
        $_SESSION['idHito'] = $hito;

        if (isset($model->plazo_entrega)) {
            $fechaFix = explode('-', $model->plazo_entrega);
            $fechaA = $fechaFix[0];
            $fechaM = $fechaFix[1];
            $fechaD = $fechaFix[2];
            $fechaFix = 'Plazo: ' . $fechaD . '-' . $fechaM . '-' . $fechaA;
        } else {
            $fechaFix = 'Sin plazo de entrega';
        }

        $_SESSION['plazo'] = $fechaFix;





        // aqui empieza el update al layout
//try ¿¿¿¿se puede hacer una funcion publica independiente de la vista???
        // fin del update al layout
//        if (strlen($model->plazo_entrega) === 0) {
//            $_SESSION['plazo'] = 'Sin plazo de entrega';
//        }

        if (isset($_SESSION['idProyecto']) || isset($id)) {
            if (isset($id)) {
                $_SESSION['idProyecto'] = $id;
            }
            ProyectosController::updateLayout($_SESSION['idProyecto'], 'update');

            $hitos = ArrayHelper::map($model->hitos, 'id', 'nombre_descriptivo');
//            if (isset($hitos)){
//                $_SESSION['idHito'] = $hitos[0];
//            }
//            $ultimoHito="?"; // (determinar cual es el hito mas cercano a vencer)
//            $primerito = $hitos[0];
//            for ($i = 0; $i<count($hitos);$i++){
            $objetivos = Yii::$app->db->createCommand(
                            'SELECT DISTINCT id_objetivo FROM objetivos_en_hitos
                             WHERE id_hito ='
                            . $hito)->queryAll();

            $dataProvider = new ActiveDataProvider([
                'query' => Objetivos::find()->where(['in', 'id', array_values(ArrayHelper::map($objetivos, 'id_objetivo', 'id_objetivo'))]),
//                        "id IN" . Yii::$app->user->getId())
            ]);

//            }
//            $objetivos = ['11','12'];
//            $objetivos = Yii::$app->db->createCommand(
//                    'SELECT DISTINCT id FROM objetivos WHERE id in (42,43)'
////                    .$objetivos
//                    )->queryAll();
//            $primerito='';
            return $this->render('continuar', [
                        'dp' => $dataProvider,
                        'id' => $hito,
                        'hitos' => $hitos,
                        'proyecto' => $model->titulo_proyecto,
            ]);
        } else {
            $this->redirect(['site/index']);
        }
    }

    public function updateLayout($id, $retorno) {
        $model = ProyectosController::findModel($id);

        $_SESSION['tipo'] = $model->tipo;

        switch ($_SESSION['tipo']) {
            case 'Personal':
                $_SESSION['tipo'] = '<i class="fa-solid fa-mug-hot"></i> ';
                break;
            case 'Estudios':
                $_SESSION['tipo'] = '<i class="fa-solid fa-graduation-cap"></i> ';
                break;
            case 'Investigación':
                $_SESSION['tipo'] = '<i class="fa-solid fa-microscope"></i> ';
                break;
            default:
                $_SESSION['tipo'] = '<i class="fa-solid fa-seedling"></i> ';
        }

        $tiempo = 0;
        $horas = 0;
        $minutos = 0;
        $segundos = 0;
        $totalHitos = 0;
        $hitosCompletados = 0;
        $totalObjetivos = 0;
        $objetivosCompletados = 0;
        $totalTareas = 0;
        $tareasCompletadas = 0;

        $hitosArray = $model->hitos;
        $totalHitos = count($hitosArray);
        foreach ($hitosArray as $hitoArray) {
            $objetivosArray = $hitoArray->objetivos;
            $totalObjetivos += count($objetivosArray);
            $completadosCount = 0;

            foreach ($objetivosArray as $objetivoArray) {
                $tareasArray = $objetivoArray->tareas;
                $totalTareas += count($tareasArray);
                $completadasCount = 0;

                foreach ($tareasArray as $tareaArray) {
                    $completadasCount += $tareaArray->completada;
                    $tareasCompletadas += $tareaArray->completada;
                    if ($completadasCount == count($tareasArray)) {
                        $objetivosCompletados++;
                        $completadosCount++;
                    }

                    foreach (ArrayHelper::getColumn($tareaArray->sesiones, 'duracion') as $time) {
                        $duracion = preg_split("/:/", $time);
                        if (count($duracion) < 2) {
                            $duracion = [0 => '00', 1 => '00', 2 => '00'];
                        }
                        $horas += intval($duracion[0]);
                        $minutos += intval($duracion[1]);
                        $segundos += intval($duracion[2]);
                    }
                }
            }
            if ($completadosCount == count($objetivosArray) && $completadosCount != 0) {
                $hitosCompletados++;
            }
        }
        $minutos += (int) ($segundos / 60);
        $horas += (int) ($minutos / 60);
        if ($horas < 10) {
            $horas = '0' . $horas;
        }
        $minutos -= 60 * (int) ($minutos / 60);
        if ($minutos < 10) {
            $minutos = '0' . $minutos;
        }
        $tiempo = 'Total: ' . $horas . ':' . $minutos/* . ' segundos: '.$segundos */;

        $_SESSION['tiempo'] = $tiempo;
        $_SESSION['totalHitos'] = $hitosCompletados . '/' . $totalHitos;
        $_SESSION['totalObjetivos'] = $objetivosCompletados . '/' . $totalObjetivos;
        $_SESSION['totalTareas'] = $tareasCompletadas . '/' . $totalTareas;

        switch ($retorno) {
            case 'horas':
                return $horas;
            case 'minutos':
                return $minutos;
            case 'segundos':
                return $segundos;
            case 'tiempo':
                return $tiempo;
            case 'hitosCompletados':
                return $hitosCompletados;
            case 'totalHitos':
                return $totalHitos;
            case 'objetivosCompletados':
                return $objetivosCompletados;
            case 'totalObjetivos':
                return $totalObjetivos;
            case 'tareasCompletadas':
                return $tareasCompletadas;
            case 'totalTareas':
                return $totalTareas;
        }
    }

//    private function tal($model) {
////        $tiempo = 0;
//        $tal = 0;
//        $horas = 0;
//        $minutos = 0;
//        $segundos = 0;
////        $sesiones = count($model->sesiones);
//
//        $hitos = $model->hitos;
//        foreach ($hitos as $hito) {
//            $objetivos = $hito->objetivos;
//            foreach ($objetivos as $objetivo) {
//                $tareas = $objetivo->tareas;
//                foreach ($tareas as $tarea) {
////                    $sesiones = $tarea->sesiones;
//                    foreach (ArrayHelper::getColumn($tarea->sesiones, 'duracion') as $time) {
//                        $duracion = preg_split("/:/", $time);
//                        if (count($duracion) < 2) {
//                            $duracion = [0 => '00', 1 => '00', 2 => '00'];
//                        }
//                        $horas += intval($duracion[0]);
//                        $minutos += intval($duracion[1]);
//                        $segundos += intval($duracion[2]);
//                    }
//                }
//            }
//        }
////print_r($model->sesiones);
////                                            $sum = $segundos/60;
//        $minutos += (int) ($segundos / 60);
//        $horas += (int) ($minutos / 60);
//        $minutos -= 60 * (int) ($minutos / 60);
//        $tal = 'horas: ' . $horas . ' minutos: ' . $minutos/* . ' segundos: '.$segundos */;
//        return $tal;
//    }

    public function actionEditar($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']]);
        }

        return $this->render('editar', [
                    'model' => $model,
        ]);
    }

    public function actionPlazo() {
        if ($_SESSION['vista'] == 'Nuevo' || $_SESSION['vista'] == 'Plazo') { //con el 'or' no devolvemos al usuario al main al recargar la pagina
            return $this->render('plazo');
        }
        return $this->render('index');
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Proyectos models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Proyectos::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Proyectos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Proyectos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Proyectos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Proyectos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Proyectos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Proyectos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Proyectos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Proyectos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
