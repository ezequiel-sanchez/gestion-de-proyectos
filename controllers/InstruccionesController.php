<?php

namespace app\controllers;

use app\models\Instrucciones;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;

/**
 * InstruccionesController implements the CRUD actions for Instrucciones model.
 */
class InstruccionesController extends Controller {

    public function actionNuevo() {
        $_SESSION['layout'] = false;
//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo' || $_SESSION['vista'] == 'Continuar') {

            $_SESSION['instruccionesCount'] = Yii::$app->db->createCommand(
                            'SELECT count(*) FROM instrucciones
                             WHERE id_tarea = ' . $_SESSION['idTarea'] . '')->queryScalar();

            $model = new Instrucciones();

            if ($this->request->isPost) {

                if ($model->load($this->request->post()) && $model->save()) {
//                    $_SESSION['idInstruccion'] = $model->id;
                    return $this->redirect(['instrucciones/nuevo']);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('nuevo', [
                        'model' => $model,
            ]);
//        }
//        return $this->redirect(["site/index"]);
    }

    public function actionEditar($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['tareas/continuar', 'id' => $_SESSION['idTarea']]);
        }

        return $this->render('editar', [
                    'model' => $model,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Instrucciones models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Instrucciones::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Instrucciones model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Instrucciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Instrucciones();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Instrucciones model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Instrucciones model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Instrucciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Instrucciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Instrucciones::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
