<?php

namespace app\controllers;

use app\models\Hitos;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * HitosController implements the CRUD actions for Hitos model.
 */
class HitosController extends Controller {

    public function actionNuevo() {
//            $_SESSION['submit']= false;
//        $_SESSION['proyecto']= $id;
        $_SESSION['layout'] = false;
        $_SESSION['continuar'] = true;
//        if ($_SESSION['vista'] == 'Main' || $_SESSION['vista'] == 'Nuevo' || $_SESSION['vista'] == 'Continuar') {
//            $id = Yii::$app->db->createCommand(
//                            'SELECT id FROM users
//                             WHERE username = "' . Yii::$app->user->identity->username . '"')->queryScalar();
//
//            $cuenta = Yii::$app->db->createCommand(
//                            'SELECT count(*) FROM proyectos
//                             WHERE id_user = (select id from users where username = "' . Yii::$app->user->identity->username . '")')->queryScalar();
//            'query' => Proyectos::find()->count()->where('username =' . Yii::$app->user->identity->username),
//                ]);
            $model = new Hitos();

//            if ($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax) {
//                Yii::$app->response->format = Response::FORMAT_JSON;
//                return ActiveForm::validate($model);
//            }
//    $_SESSION['submit']= 0;
            if ($this->request->isPost) {
//                if ($_SESSION['submit']=== 1){
                if ($model->load($this->request->post()) && $model->save()) {
//                    return $this->redirect(['view', 'id' => $model->id]);
                    $_SESSION['idHito'] = $model->id;
                    return $this->redirect(['objetivos/nuevo']);
                }
//                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('nuevo', [
                        'model' => $model,
//                        'cuenta' => $cuenta + 1,
//                        'id' => $id,
            ]);
//        }
//        return $this->redirect(["site/index"]);
    }

    public function actionEditar($id) {
        $model = $this->findModel($id);
        $_SESSION['layout'] = true;
        $_SESSION['idHito'] = $id;
        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['proyectos/continuar', 'id' => $_SESSION['idProyecto'], 'hito' => $_SESSION['idHito']]);
        }

        return $this->render('editar', [
                    'model' => $model,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Hitos models.
     *
     * @return string
     */
    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Hitos::find(),
                /*
                  'pagination' => [
                  'pageSize' => 50
                  ],
                  'sort' => [
                  'defaultOrder' => [
                  'id' => SORT_DESC,
                  ]
                  ],
                 */
        ]);

        return $this->render('index', [
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Hitos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Hitos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Hitos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Hitos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Hitos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Hitos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Hitos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Hitos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
