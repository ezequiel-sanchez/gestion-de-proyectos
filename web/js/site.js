/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function mostrar(actualId, siguienteId) {
    var actual = document.getElementById(actualId);
    var siguiente = document.getElementById(siguienteId);
    actual.className = 'd-none';


    var formulario = document.getElementById('submit');
    if (null !== formulario) {
        formulario.focus();
    }
    if (siguienteId === 'submit') {
        siguiente.className = 'text-center';
        submit();
    } else {
        if (null !== siguiente) {
            siguiente.className = 'op80 bgb text-center';
            siguiente.focus();
        }
    }
}

function generico(actualId, siguienteId, campoId, generico, cuenta) {
    var campo = document.getElementById(campoId + '-campo');

    if (generico === 'Plazo') {
    } else {
        campo.value = generico + " " + cuenta;
    }
    mostrar(actualId, siguienteId);
}

function siguiente(actualId, siguienteId) {
    var flag = false;
    var campo = document.getElementById(actualId + '-campo');

    setTimeout(() => {
        if (campo.getAttribute('aria-invalid') === 'true') {
//            console.log("❌");
        } else {
            if (actualId === "fecha") {
                var error = document.getElementById(actualId + '-error');
                var fechahoy = fechaHoy().split('-');
                // esto hace que se rompa la comprobacion si es el ultimo dia del mes >.<
//                fechahoy[2]++;
                var hoy = new Date(fechahoy);
                var plazo = new Date(campo.value.toString().split('-'));

                if (hoy > plazo) {
//                    console.log("hoy mayor que plazo");
                    error.textContent = "El plazo no puede ser anterior o igual que hoy";
                    error.className = "w-50 text-center invalid-feedback d-block";
                } else {
                    flag = true;
                }

                if (flag) {
                    mostrar(actualId, siguienteId);
                }
            } else {
                mostrar(actualId, siguienteId);
            }

        }
    }, 250);
}


function dropList(actualId, siguienteId) {

    var lista = document.getElementById(actualId + '-list');
    var campo = document.getElementById(actualId + '-campo');

    campo.value = lista.value;
    mostrar(actualId, siguienteId);
}

function fechaHoy() {
    var date = new Date();
    var año = date.getFullYear();
    var mes = date.getMonth() + 1;
    var dia = date.getDate();

    if (mes < 10) {
        var mes = "0" + mes;
    }

    if (dia < 10) {
        var dia = "0" + dia;
    }

    var fecha = año + "-" + mes + "-" + dia;
    return fecha;
}

function ahora() {
    var ahora = new Date();
    var h = ahora.getHours();
    var m = ahora.getMinutes();
    var s = ahora.getSeconds();
    if (m < 10) {
        var m = "0" + m;
    }

    if (s < 10) {
        var s = "0" + s;
    }
    var hora = h + ":" + m + ":" + s;
    return hora;
}

function fixFecha(fecha, id) {
//    console.log('tal');
//    console.log(fecha);
//    console.log(id);
//    console.log('cual');
    if (fecha !== null) {
        var campo = document.getElementById('fecha-' + id);
        var fechaFix = new Date(fecha.toString().split('-'));
        var fechaStr = fechaFix.getDate() + '-' + fechaFix.getMonth() + '-' + fechaFix.getFullYear();
        campo.textContent = fechaStr;
    }
}

function rellenoProyecto(id) {
    var fechaInicial = document.getElementById('fechaInicial');
    var userId = document.getElementById('userId');
    var estado = document.getElementById('estado');
    fechaInicial.value = fechaHoy();
    userId.value = id;
    estado.value = 1;
}

function rellenoHito(id) {
    var proyectoId = document.getElementById('proyectoId');
    proyectoId.value = id;
}

//Productos A Obtener
function rellenoPAO(id) {
    var objetivoId = document.getElementById('objetivoId');
    objetivoId.value = id;
}

//Objetivos En Hitos
function rellenoOEH(idObjetivo, idHito) {
    var objetivoId = document.getElementById('objetivoId');
    var hitoId = document.getElementById('hitoId');
    objetivoId.value = idObjetivo;
    hitoId.value = idHito;
    submit();
}

function rellenoTarea(id) {
    var objetivoId = document.getElementById('objetivoId');
    var estado = document.getElementById('estado');
    objetivoId.value = id;
    estado.value = 0;
}

function rellenoInstrucciones(id) {
    var tareaId = document.getElementById('tareaId');
    tareaId.value = id;
}

function submit() {
    var submit = document.getElementById('submit-button');
    setTimeout(() => {
        submit.click();
    }, 1200);
}

function continuar(id) {
    var continuar = document.getElementById(id);
    continuar.click();
}

function empty() {
    var panel = document.getElementById("w0");
    var empty = document.getElementsByClassName("empty");
    var vacio = document.getElementById("vacio");

    if (empty.length === 0) {
//        console.log('vacio (si hay objetivos)');
    } else {
//        console.log('no vacio (no hay objetivos)');
        panel.innerHTML = vacio.innerHTML;
        panel.className = panel.className + " mt-3";
    }
}

function vista(condicion) {
    var boton = document.getElementById("continuar");
    if (condicion === 'Continuar') {
        boton.click();
    }
}

function completada() {
    var tareaId = document.getElementById('completada');
    if (tareaId.value == 0) {
        tareaId.value = 1;
    } else {
        tareaId.value = 0;
    }
    submit();
}

function rellenoIniciarSesion(idTarea) {
    var tareaId = document.getElementById('tareaId');
    var inicio = document.getElementById('hInicio');
    tareaId.value = idTarea;
    inicio.value = fechaHoy() + " " + ahora();
    submit();
}

function rellenoTerminarSesion() {
    var duracion = document.getElementById('duracion');
    var inicio = document.getElementById('hInicio');
    var fin = document.getElementById('hFin');
    fin.value = fechaHoy() + " " + ahora();
    var i = new Date(inicio.value);
    var f = new Date(fin.value);
    var n = f - i;

    var horas = parseInt(((n / 1000) / 60) / 60);
    var minutos = parseInt(((n / 1000) / 60) - (60 * horas));
    var segundos = parseInt(((n / 1000) - (60 * 60 * horas)) - (60 * minutos));

    if (horas < 10) {
        var horas = "0" + horas;
    }

    if (minutos < 10) {
        var minutos = "0" + minutos;
    }

    if (segundos < 10) {
        var segundos = "0" + segundos;
    }
    duracion.value = horas + ':' + minutos + ':' + segundos;
//    console.log('inicio');
//    console.log(i);
//    console.log('fin');
//    console.log(f);
//    console.log('dif');
//    console.log();
//    hora fin.
//    resumen de trabajo y mas na.
//    var objetivoId = document.getElementById('objetivoId');
//    var hitoId = document.getElementById('hitoId');
//    objetivoId.value = idObjetivo;
//    hitoId.value = idHito;
    submit();
}

function debug(tal) {
    console.log(' ___DEBUG:___ ');
    console.log('___________________________');
    console.log('🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻🔻');
    console.log(tal);
    console.log('🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺🔺');
    console.log('___________________________');

}



//CRONOMETRO JS

function init(trabajando, duracion) {
    var tiempo = duracion.toString().split(":");
    var hora = ahora().toString().split(":");
    var tiempoDif = (parseInt(tiempo[0]) * 60 * 60) + (parseInt(tiempo[1])) * 60 + parseInt(tiempo[2]);
    var ahoraDif = (parseInt(hora[0]) * 60 * 60) + (parseInt(hora[1])) * 60 + parseInt(hora[2]);
    var dif = ahoraDif - tiempoDif;

//    setTimeout(() => {
//        var iframe = document.getElementById("yt");
//        var dos = document.getElementById('player');
////        var marca = document.getElementsByClassName('annotation');
////        var els = document.getElementsByClassName(".iv-branding .branding-img-container img");
////        var els = document.querySelector("#movie_player > div.ytp-player-content.ytp-iv-player-content > div.annotation.annotation-type-custom.iv-branding > button > img");
////        $(els[els.length - 1]).remove();
////        marca = ['a', 'b', 'c'];
////        marca.forEach(element => console.log(element));
////console.log('ini');
////console.log(iframe);
////iframe.content().find('#player').css('display','none');
////console.log(iframe);
////marca.classname="tal ";
////dos.classname="tal ";
////dos.remove();
////$('.iv-branding .branding-img-container img').last().remove();
////console.log('fin');
//    }, 5000);

    tiempo[0] = parseInt((dif / 60) / 60);
    tiempo[1] = parseInt((dif / 60) - (60 * tiempo[0]));
    tiempo[2] = parseInt((dif - (tiempo[0] * 60) * 60) - (tiempo[1] * 60));
    document.querySelector(".start").addEventListener("click", cronometrar);
    document.querySelector(".stop").addEventListener("click", parar);
    document.querySelector(".reiniciar").addEventListener("click", reiniciar);

    h = tiempo[0];
    m = tiempo[1];
    s = tiempo[2];

    mh = h;
    mm = m;
    ms = s;

    if (s < 10) {
        ms = '0' + s;
    }
    if (m < 10) {
        mm = '0' + m;
    }
    if (h < 10) {
        mh = '0' + h;
    }

    if (trabajando > 0) {
//        console.log('trabajando');
        cronometrar();
    } else {
//        console.log('descansando');
//        parar();
        mh = '00';
        mm = '00';
        ms = '00';
    }
//    console.log(dif);

//var tiempo = duracion.toString().split(":");
//console.log(tiempo);



    document.getElementById("hms").innerHTML = mh + ':' + mm + ':' + ms;
}
function cronometrar() {
    escribir();
    id = setInterval(escribir, 1000);
    document.querySelector(".start").removeEventListener("click", cronometrar);
}
function escribir() {
    var botonParar = document.getElementById('boton-parar');
    var hAux, mAux, sAux;
    s++;
    if (s > 59) {
        m++;
        s = 0;
    }
    if (m > 59) {
        h++;
        m = 0;
    }
    if (h > 24) {
        botonParar.click();
    }

    if (s < 10) {
        sAux = "0" + s;
    } else {
        sAux = s;
    }
    if (m < 10) {
        mAux = "0" + m;
    } else {
        mAux = m;
    }
    if (h < 10) {
        hAux = "0" + h;
    } else {
        hAux = h;
    }

    document.getElementById("hms").innerHTML = hAux + ":" + mAux + ":" + sAux;
}
function parar() {
    clearInterval(id);
    document.querySelector(".start").addEventListener("click", cronometrar);

}
function reiniciar() {
    clearInterval(id);
    document.getElementById("hms").innerHTML = "00:00:00";
    h = 0;
    m = 0;
    s = 0;
    document.querySelector(".start").addEventListener("click", cronometrar);
}

function fin(trabajando, completada) {
    var botonParar = document.getElementById('boton-parar');
    if (completada && trabajando) {
        botonParar.click();
    }
}

function demo() {
    var campoUser = document.getElementById('loginform-usuario');
    var campoPass = document.getElementById('loginform-contraseña');
    var botonAcceder = document.getElementById('ux-login');
    campoPass.className = campoPass.className + ' text-white';
    campoUser.className = campoUser.className + ' text-white';
    campoUser.value = 'demo';
    campoPass.value = 'fordemo';
    botonAcceder.click();
}


function ambience(dir, elemento) {
    var Sys_Ambience1 = 'http://localhost/' + dir + '/web/audio/Env_FirstCave.wav';
    var Sys_Ambience2 = 'http://localhost/' + dir + '/web/audio/Env_WindDirect.wav';
    var Ambience1 = new Audio(Sys_Ambience1);
    var Ambience2 = new Audio(Sys_Ambience2);
    Ambience1.volume = 0.18;
    Ambience2.volume = 0.05;
    Ambience1.preload = 'auto';
    Ambience2.preload = 'auto';


//    console.log(elemento);
    if (elemento !== '1') {
//        Ambience1.muted = true;
        Ambience1.play();
        setInterval(() => {
            Ambience1.currentTime = 0;
        }, 5075);
    } else {
//        Ambience2.muted = true;
        Ambience2.play();
        setInterval(() => {
            Ambience2.currentTime = 0;
        }, 2978);
    }

}

function seleccionar() {
    var elemento = document.querySelector("#w0 > div:nth-child(4) > div:nth-child(2) > div > a");
    elemento.focus();
}

function hoverSound(dir, elemento) {
    var Sys_HoverBig = 'http://localhost/' + dir + '/web/audio/Sys_HoverBig.wav';
    var Sys_HoverSmall = 'http://localhost/' + dir + '/web/audio/Sys_HoverSmall.wav';
    var Sys_MapHover = 'http://localhost/' + dir + '/web/audio/Sys_MapHover.wav';
    var Msg_Receive = 'http://localhost/' + dir + '/web/audio/Msg_Receive.wav';
    var Msg_BlueFire_BurnStart = 'http://localhost/' + dir + '/web/audio/Msg_BlueFire_BurnStart.wav';
    var Sys_Extra_Click = 'http://localhost/' + dir + '/web/audio/Sys_Extra_Click.wav';

    var HoverBig = new Audio(Sys_HoverBig);
    HoverBig.volume = 0.1;
    var HoverSmall = new Audio(Sys_HoverSmall);
    HoverSmall.volume = 0.05;
    var MapHover = new Audio(Sys_MapHover);
    MapHover.volume = 0.3;
    var Receive = new Audio(Msg_Receive);
    Receive.volume = 0.3;
    var BlueFire_BurnStart = new Audio(Msg_BlueFire_BurnStart);
    BlueFire_BurnStart.volume = 0.1;
    var Extra_Click = new Audio(Sys_Extra_Click);
    Extra_Click.volume = 0.1;

//console.log(sessionStorage.getItem('once'));
//if (sessionStorage.getItem('once') !== 0 ){
//    sessionStorage.setItem('once', 0);
//}

    switch (elemento) {
        case 'HoverBig':
            HoverBig.play();
            break;
        case 'HoverSmall':
            HoverSmall.play();
            break;
        case 'MapHover':
            MapHover.play();
            break;
        case 'Receive':
            Receive.play();
            break;
        case 'BlueFire_BurnStart':
            BlueFire_BurnStart.play();
            break;
        case 'Extra_Click':
//            if (sessionStorage.getItem('once') === '0') {
            Extra_Click.play();
//                sessionStorage.setItem('once', 1);
//            }
            break;
    }



}

function startGame(dir) {
    var canvas = document.querySelector("body > div > div > main > div");
    var Sys_GameStart = 'http://localhost/' + dir + '/web/audio/Sys_GameStart.wav';
    var GameStart = new Audio(Sys_GameStart);

    canvas.className = 'd-none';
    GameStart.volume = 0.3;
    GameStart.play();
//    console.log('hola?');
}

function clickSound(dir, elemento, id, ms) {
    var mapa = document.querySelector("body > div > div > header > div > div");
    var canvas = document.querySelector("body > div > div > main > div");
    var a = document.getElementById(id);

    var Msg_BlueFire_BurnStart = 'http://localhost/' + dir + '/web/audio/Msg_BlueFire_BurnStart.wav';
    var Msg_Sent = 'http://localhost/' + dir + '/web/audio/Msg_Sent.wav';
    var Msg_Receive = 'http://localhost/' + dir + '/web/audio/Msg_Receive.wav';
    var Sys_Back = 'http://localhost/' + dir + '/web/audio/Sys_Back.wav';
    var Sys_Extra_Click = 'http://localhost/' + dir + '/web/audio/Sys_Extra_Click.wav';
    var Sys_HoverBig_Click = 'http://localhost/' + dir + '/web/audio/Sys_HoverBig_Click.wav';
    var Sys_HoverSmall_Click = 'http://localhost/' + dir + '/web/audio/Sys_HoverSmall_Click.wav';
    var Sys_Invalid = 'http://localhost/' + dir + '/web/audio/Sys_Invalid.wav';
    var Sys_MapClick = 'http://localhost/' + dir + '/web/audio/Sys_MapClick.wav';
    var Event_MapWarpIn = 'http://localhost/' + dir + '/web/audio/Event_MapWarpIn.wav';
    var Sys_Mod_Close = 'http://localhost/' + dir + '/web/audio/Sys_Mod_Close.wav';
    var Sys_Mod_Open = 'http://localhost/' + dir + '/web/audio/Sys_Mod_Open.wav';
    var Sys_Talk_End = 'http://localhost/' + dir + '/web/audio/Sys_Talk_End.wav';
    var Sys_Talk_Next = 'http://localhost/' + dir + '/web/audio/Sys_Talk_Next.wav';
    var Sys_Talk_No = 'http://localhost/' + dir + '/web/audio/Sys_Talk_No.wav';

    var BlueFire_BurnStart = new Audio(Msg_BlueFire_BurnStart);
    BlueFire_BurnStart.volume = 0.7;
    var Sent = new Audio(Msg_Sent);
    Sent.volume = 0.5;
    var Receive = new Audio(Msg_Receive);
    Receive.volume = 0.35;
    var Back = new Audio(Sys_Back);
    Back.volume = 0.35;
    var Extra_Click = new Audio(Sys_Extra_Click);
    Extra_Click.volume = 0.3;
    var HoverBig_Click = new Audio(Sys_HoverBig_Click);
    HoverBig_Click.volume = 0.3;
    var HoverSmall_Click = new Audio(Sys_HoverSmall_Click);
    HoverSmall_Click.volume = 0.3;
    var Invalid = new Audio(Sys_Invalid);
    Invalid.volume = 0.3;
    var MapClick = new Audio(Sys_MapClick);
    MapClick.volume = 0.3;
    var MapWarpIn = new Audio(Event_MapWarpIn);
    MapWarpIn.volume = 0.3;
    var Mod_Close = new Audio(Sys_Mod_Close);
    Mod_Close.volume = 0.3;
    var Mod_Open = new Audio(Sys_Mod_Open);
    Mod_Open.volume = 0.3;
    var Talk_End = new Audio(Sys_Talk_End);
    Talk_End.volume = 0.3;
    var Talk_Next = new Audio(Sys_Talk_Next);
    Talk_Next.volume = 0.3;
    var Talk_No = new Audio(Sys_Talk_No);
    Talk_No.volume = 0.3;

    
    if (id === 'login' || id === 'recuperar') {
        updatePos();
        if (document.querySelector("#w0 > div.field.my-3 > div > div.invalid-feedback").textContent !== '') {
            elemento = 'Invalid';
        }
    }

    if (id === 'registro') {
        updatePos();
        if (document.querySelector("#formulario > div:nth-child(2) > div > div.invalid-feedback").textContent !== '' ||
                document.querySelector("#formulario > div:nth-child(3) > div > div.invalid-feedback").textContent !== '' ||
                document.querySelector("#formulario > div:nth-child(4) > div > div.invalid-feedback").textContent !== '' ||
                document.querySelector("#formulario > div:nth-child(5) > div > div.invalid-feedback").textContent !== '') {
            elemento = 'Invalid';
        }
    }

    switch (elemento) {
        case 'BlueFire_BurnStart':
            BlueFire_BurnStart.play();
            break;
        case 'Sent':
            Sent.play();
            break;
        case 'Receive':
            Receive.play();
            break;
        case 'Back':
            Back.play();
            break;
        case 'Extra_Click':
            Extra_Click.play();
            break;
        case 'HoverBig_Click':
            HoverBig_Click.play();
            break;
        case 'HoverSmall_Click':
            HoverSmall_Click.play();
            break;
        case 'Invalid':
            Invalid.play();
            break;
        case 'MapClick':
            MapClick.play();
            setTimeout(() => {
                canvas.className = 'd-none';
                mapa.className = 'invisible';
                warp();
//                setTimeout(() => {
                MapWarpIn.play();
//                    esconder canvas
//                }, 5000);
            }, 600);

            break;
        case 'Mod_Close':
            Mod_Close.play();
            break;
        case 'Mod_Open':
            Mod_Open.play();
            break;
        case 'Talk_End':
            Talk_End.play();
            break;
        case 'Talk_Next':
            Talk_Next.play();
            break;
        case 'Talk_No':
            Talk_No.play();
            break;
    }

    if (id !== 'null') {
        setTimeout(() => {
            a.click();
        }, ms);
    }
}



var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;

var pos;
function onYouTubeIframeAPIReady() {
//    sessionStorage.setItem('volumenYt', 2);
    player = new YT.Player('player', {
        height: '360',
        width: '640',
        videoId: 'L6uXNrQZNQU',
        startSeconds: pos,
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        }
    });
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
//    console.log('awiwi');
//    console.log(sessionStorage.getItem('intro'));
    player.setVolume(parseInt(sessionStorage.getItem('volumenYt')));
    if (sessionStorage.getItem('intro') === '1') {
        event.target.playVideo();
        player.seekTo(sessionStorage.getItem('pos'));
        sessionStorage.setItem('intro', 0);
    }
//    console.log(sessionStorage.getItem('intro'));
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
//        setTimeout(stopVideo, 6000);
        pos = player.getCurrentTime() + 5;
        done = true;
    }
}
function stopVideo() {
    player.stopVideo();
}

function updatePos() {
    sessionStorage.setItem('pos', player.getCurrentTime());
//    console.log(player);
}

function ajustarVolumen(volumen) {
//    console.log('mira');
    sessionStorage.setItem('volumenYt', volumen);
//    console.log(volumen);
}

function introAutoPlay() {
    sessionStorage.setItem('intro', 1);
}

//PARTICULAS
function updateParticulas() {
//    sessionStorage.setItem('cantidad', 10);
//    sessionStorage.setItem('direccion', "right");
//    sessionStorage.setItem('velocidad', 1);
//    sessionStorage.setItem('modo', "bubble");

//particlesJS
//        (
//                "particles-js", {
//                    "particles": {
//                        "number": {
//                            "value": sessionStorage.getItem('cantidad'),
//                            "density": {
//                                "enable": true,
//                                "value_area": 800
//                            }
//                        },
//                        "color": {
//                            "value": "#FFF3C7"
//                        },
//
//                        "shape": {
//                            "type": "circle",
//                            "stroke": {"width": 0, "color": "#000000"},
//                            "polygon": {"nb_sides": 5},
//                            "image": {"src": "img/github.svg", "width": 100, "height": 100}
//                        },
//
//                        "opacity": {
//                            "value": 0.5,
//                            "random": false, "anim": {
//                                "enable": false,
//                                "speed": 1,
//                                "opacity_min": 0.1,
//                                "sync": false}
//                        },
//
//                        "size": {
//                            "value": 3,
//                            "random": true,
//                            "anim": {
//                                "enable": false,
//                                "speed": 40,
//                                "size_min": 0.1,
//                                "sync": false
//                            }
//                        },
//
//                        "line_linked": {
//                            "enable": false,
//                            "distance": 150,
//                            "color": "#FFF3C7",
//                            "opacity": 0.4,
//                            "width": 1
//                        },
//
//                        "move": {
//                            "enable": true,
//                            "speed": sessionStorage.getItem('velocidad'),
//                            "direction": sessionStorage.getItem('direccion'),
//                            "random": false,
//                            "straight": false,
//                            "out_mode": "out",
//                            "bounce": false,
//                            "attract": {
//                                "enable": true,
//                                "rotateX": 600,
//                                "rotateY": 1200}
//                        }
//                    },
//
//                    "interactivity": {
//                        "detect_on": "window",
//                        "events": {
//                            "onhover": {
//                                "enable": true,
//                                "mode": "bubble"
//                            }, "onclick": {
//                                "enable": false,
//                                "mode": "push"
//                            }, "resize": true
//                        },
//
//                        "modes": {
//                            "grab": {
//                                "distance": 400,
//                                "line_linked": {
//                                    "opacity": 1
//                                }
//                            },
//                            "bubble": {
//                                "distance": 230,
//                                "size": 4,
//                                "duration": 4,
//                                "opacity": 0.3,
//                                "speed": 3
//                            },
//                            "repulse": {
//                                "distance": 80,
//                                "duration": 0.4
//                            },
//                            "push": {
//                                "particles_nb": 4
//                            },
//                            "remove": {
//                                "particles_nb": 2}
//                        }
//                    },
//                    "retina_detect": false
//                }
//        );
}



//var count_particles, stats, update;
//stats = new Stats;
//stats.setMode(0);
//stats.domElement.style.position = 'absolute';
//stats.domElement.style.left = '0px';
//stats.domElement.style.top = '0px';
//document.body.appendChild(stats.domElement);
//count_particles = document.querySelector('.js-count-particles');
//
//update = function () {
//    stats.begin();
//    stats.end();
//    if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
//        count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
//    }
//    requestAnimationFrame(update);
//};
//requestAnimationFrame(update);
//;

function warp() {
    particlesJS.load('particles-js', '../../assets/particlesWarp.json', function () {
//  console.log('callback - particles.js config loaded');
    });

//    sessionStorage.setItem('cantidad', 120);
//    sessionStorage.setItem('direccion', "top");
//    sessionStorage.setItem('velocidad', 100);
//    sessionStorage.setItem('modo', "repulse");
}

//function chispas() {
//    particlesJS.load('particles-js-f1', '../../assets/particlesFire.json', function () {
////  console.log('callback - particles.js config loaded');
//    });
//
//    particlesJS.load('particles-js-f2', '../../assets/particlesFire2.json', function () {
////  console.log('callback - particles.js config loaded');
//    });
//}


function particulasIndex() {
    particlesJS.load('particles-js', 'assets/particles.json', function () {
  console.log('callback - particles.js config loaded');
    });
}

//console.log(document.title);
//if (document.title !== 'Main'){
//    console.log(document.title);
particlesJS.load('particles-js', '../../assets/particles.json', function () {
//  console.log('callback - particles.js config loaded');
});
//}


