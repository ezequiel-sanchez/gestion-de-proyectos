<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetivos_en_hitos".
 *
 * @property int $id
 * @property int $id_objetivo
 * @property int $id_hito
 *
 * @property Hitos $hito
 * @property Objetivos $objetivo
 */
class ObjetivosEnHitos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetivos_en_hitos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_objetivo', 'id_hito'], 'required'],
            [['id_objetivo', 'id_hito'], 'integer'],
            [['id_objetivo', 'id_hito'], 'unique', 'targetAttribute' => ['id_objetivo', 'id_hito']],
            [['id_hito'], 'exist', 'skipOnError' => true, 'targetClass' => Hitos::className(), 'targetAttribute' => ['id_hito' => 'id']],
            [['id_objetivo'], 'exist', 'skipOnError' => true, 'targetClass' => Objetivos::className(), 'targetAttribute' => ['id_objetivo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_objetivo' => 'Id Objetivo',
            'id_hito' => 'Id Hito',
        ];
    }

    /**
     * Gets query for [[Hito]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHito()
    {
        return $this->hasOne(Hitos::className(), ['id' => 'id_hito']);
    }

    /**
     * Gets query for [[Objetivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivo()
    {
        return $this->hasOne(Objetivos::className(), ['id' => 'id_objetivo']);
    }
}
