<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrucciones".
 *
 * @property int $id
 * @property int $id_tarea
 * @property string $paso
 *
 * @property Tareas $tarea
 */
class Instrucciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrucciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tarea', 'paso'], 'required'],
            [['id_tarea'], 'integer'],
            [['paso'], 'string', 'max' => 300],
            [['id_tarea', 'paso'], 'unique', 'targetAttribute' => ['id_tarea', 'paso']],
            [['id_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => Tareas::className(), 'targetAttribute' => ['id_tarea' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tarea' => 'Id Tarea',
            'paso' => 'Paso',
        ];
    }

    /**
     * Gets query for [[Tarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarea()
    {
        return $this->hasOne(Tareas::className(), ['id' => 'id_tarea']);
    }
}
