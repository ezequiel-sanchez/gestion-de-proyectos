<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objetivos".
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $descripcion
 *
 * @property Hitos[] $hitos
 * @property ObjetivosEnHitos[] $objetivosEnHitos
 * @property ProductosAObtener[] $productosAObteners
 * @property Tareas[] $tareas
 */
class Objetivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objetivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['titulo'], 'required'],
            [['titulo'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 300],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
        ];
    }

    /**
     * Gets query for [[Hitos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHitos()
    {
        return $this->hasMany(Hitos::className(), ['id' => 'id_hito'])->viaTable('objetivos_en_hitos', ['id_objetivo' => 'id']);
    }

    /**
     * Gets query for [[ObjetivosEnHitos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivosEnHitos()
    {
        return $this->hasMany(ObjetivosEnHitos::className(), ['id_objetivo' => 'id']);
    }

    /**
     * Gets query for [[ProductosAObteners]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosAObteners()
    {
        return $this->hasMany(ProductosAObtener::className(), ['id_objetivo' => 'id']);
    }

    /**
     * Gets query for [[Tareas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTareas()
    {
        return $this->hasMany(Tareas::className(), ['id_objetivo' => 'id']);
    }
}
