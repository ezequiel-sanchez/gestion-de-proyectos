<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proyectos".
 *
 * @property int $id
 * @property string $titulo_proyecto
 * @property string $fecha_inicio
 * @property string|null $fecha_fin
 * @property string|null $plazo_entrega
 * @property int $id_user
 * @property string $tipo
 * @property int $estado
 *
 * @property Hitos[] $hitos
 * @property Users $user
 */
class Proyectos extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'proyectos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['titulo_proyecto', 'fecha_inicio', 'id_user', 'tipo'], 'required', 'message' => 'Campo requerido'],
            [['fecha_inicio', 'fecha_fin', 'plazo_entrega'], 'safe'],
            [['id_user', 'estado'], 'integer'],
            [['titulo_proyecto', 'tipo'], 'string', 'max' => 20],
//            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'titulo_proyecto' => 'Titulo Proyecto',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
            'plazo_entrega' => 'Plazo Entrega',
            'id_user' => 'Id User',
            'tipo' => 'Tipo',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Hitos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHitos() {
        return $this->hasMany(Hitos::className(), ['id_proyecto' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(Users::className(), ['id' => 'id_user']);
    }

}
