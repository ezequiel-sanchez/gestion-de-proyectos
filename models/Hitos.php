<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "hitos".
 *
 * @property int $id
 * @property string|null $nombre_descriptivo
 * @property string|null $fecha_limite
 * @property int $id_proyecto
 *
 * @property Objetivos[] $objetivos
 * @property ObjetivosEnHitos[] $objetivosEnHitos
 * @property Proyectos $proyecto
 */
class Hitos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hitos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_limite'], 'safe'],
            [['id_proyecto'], 'required'],
            [['id_proyecto'], 'integer'],
            [['nombre_descriptivo'], 'string', 'max' => 20],
            [['id_proyecto'], 'exist', 'skipOnError' => true, 'targetClass' => Proyectos::className(), 'targetAttribute' => ['id_proyecto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre_descriptivo' => 'Nombre Descriptivo',
            'fecha_limite' => 'Fecha Limite',
            'id_proyecto' => 'Id Proyecto',
        ];
    }

    /**
     * Gets query for [[Objetivos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivos()
    {
        return $this->hasMany(Objetivos::className(), ['id' => 'id_objetivo'])->viaTable('objetivos_en_hitos', ['id_hito' => 'id']);
    }

    /**
     * Gets query for [[ObjetivosEnHitos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivosEnHitos()
    {
        return $this->hasMany(ObjetivosEnHitos::className(), ['id_hito' => 'id']);
    }

    /**
     * Gets query for [[Proyecto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProyecto()
    {
        return $this->hasOne(Proyectos::className(), ['id' => 'id_proyecto']);
    }
}
