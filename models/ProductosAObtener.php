<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_a_obtener".
 *
 * @property int $id
 * @property int $id_objetivo
 * @property string $resultado
 *
 * @property Objetivos $objetivo
 */
class ProductosAObtener extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_a_obtener';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_objetivo', 'resultado'], 'required'],
            [['id_objetivo'], 'integer'],
            [['resultado'], 'string', 'max' => 300],
            [['id_objetivo', 'resultado'], 'unique', 'targetAttribute' => ['id_objetivo', 'resultado']],
            [['id_objetivo'], 'exist', 'skipOnError' => true, 'targetClass' => Objetivos::className(), 'targetAttribute' => ['id_objetivo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_objetivo' => 'Id Objetivo',
            'resultado' => 'Resultado',
        ];
    }

    /**
     * Gets query for [[Objetivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivo()
    {
        return $this->hasOne(Objetivos::className(), ['id' => 'id_objetivo']);
    }
}
