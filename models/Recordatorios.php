<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recordatorios".
 *
 * @property int $id
 * @property string $fecha
 * @property string $hora
 * @property string $mensaje
 * @property int|null $id_tarea
 *
 * @property Tareas $tarea
 */
class Recordatorios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recordatorios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora', 'mensaje'], 'required'],
            [['fecha', 'hora'], 'safe'],
            [['id_tarea'], 'integer'],
            [['mensaje'], 'string', 'max' => 300],
            [['id_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => Tareas::className(), 'targetAttribute' => ['id_tarea' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'mensaje' => 'Mensaje',
            'id_tarea' => 'Id Tarea',
        ];
    }

    /**
     * Gets query for [[Tarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarea()
    {
        return $this->hasOne(Tareas::className(), ['id' => 'id_tarea']);
    }
}
