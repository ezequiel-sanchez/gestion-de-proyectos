<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sesiones".
 *
 * @property int $id
 * @property string|null $duracion
 * @property string $hora_inicio
 * @property string|null $hora_fin
 * @property string|null $resumen_trabajo
 * @property int $id_tarea
 *
 * @property Tareas $tarea
 */
class Sesiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sesiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion', 'hora_inicio', 'hora_fin'], 'safe'],
            [['hora_inicio', 'id_tarea'], 'required'],
            [['id_tarea'], 'integer'],
            [['resumen_trabajo'], 'string', 'max' => 300],
            [['id_tarea'], 'exist', 'skipOnError' => true, 'targetClass' => Tareas::className(), 'targetAttribute' => ['id_tarea' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'duracion' => 'Duracion',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
            'resumen_trabajo' => 'Resumen Trabajo',
            'id_tarea' => 'Id Tarea',
        ];
    }

    /**
     * Gets query for [[Tarea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTarea()
    {
        return $this->hasOne(Tareas::className(), ['id' => 'id_tarea']);
    }
}
