<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tareas".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $descripcion
 * @property int $completada
 * @property int $id_objetivo
 *
 * @property Instrucciones[] $instrucciones
 * @property Objetivos $objetivo
 * @property Recordatorios[] $recordatorios
 * @property Sesiones[] $sesiones
 */
class Tareas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tareas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'completada', 'id_objetivo'], 'required'],
            [['completada', 'id_objetivo'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['descripcion'], 'string', 'max' => 300],
            [['id_objetivo'], 'exist', 'skipOnError' => true, 'targetClass' => Objetivos::className(), 'targetAttribute' => ['id_objetivo' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'completada' => 'Completada',
            'id_objetivo' => 'Id Objetivo',
        ];
    }

    /**
     * Gets query for [[Instrucciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstrucciones()
    {
        return $this->hasMany(Instrucciones::className(), ['id_tarea' => 'id']);
    }

    /**
     * Gets query for [[Objetivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjetivo()
    {
        return $this->hasOne(Objetivos::className(), ['id' => 'id_objetivo']);
    }

    /**
     * Gets query for [[Recordatorios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRecordatorios()
    {
        return $this->hasMany(Recordatorios::className(), ['id_tarea' => 'id']);
    }

    /**
     * Gets query for [[Sesiones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSesiones()
    {
        return $this->hasMany(Sesiones::className(), ['id_tarea' => 'id']);
    }
}
