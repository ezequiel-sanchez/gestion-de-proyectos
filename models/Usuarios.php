<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $user
 * @property string $pass
 * @property string $mail
 * @property string $auth_key
 * @property string $access_token
 * @property int $estado
 *
 * @property Proyectos[] $proyectos
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user', 'pass', 'mail', 'auth_key', 'access_token'], 'required'],
            [['estado'], 'integer'],
            [['user'], 'string', 'max' => 50],
            [['pass', 'auth_key', 'access_token'], 'string', 'max' => 250],
            [['mail'], 'string', 'max' => 80],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user' => 'User',
            'pass' => 'Pass',
            'mail' => 'Mail',
            'auth_key' => 'Auth Key',
            'access_token' => 'Access Token',
            'estado' => 'Estado',
        ];
    }

    /**
     * Gets query for [[Proyectos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProyectos()
    {
        return $this->hasMany(Proyectos::className(), ['id_user' => 'id']);
    }
}
